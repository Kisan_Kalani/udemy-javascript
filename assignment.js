let Fundamental_1_Coding_Challenge_1
// Coding Challenge #1
// Mark and John are trying to compare their BMI (Body Mass Index), which is
// calculated using the formula:
// BMI = mass / height ** 2 = mass / (height * height) (mass in kg
// and height in meter).
// Your tasks:
// 1. Store Mark's and John's mass and height in variables
// 2. Calculate both their BMIs using the formula (you can even implement both
// versions)
// 3. Create a Boolean variable 'markHigherBMI' containing information about
// whether Mark has a higher BMI than John.
// Test data:
// § Data 1: Marks weights 78 kg and is 1.69 m tall. John weights 92 kg and is 1.95
// m tall.
// § Data 2: Marks weights 95 kg and is 1.88 m tall. John weights 85 kg and is 1.76m tall.

// ------------SOLUTION------------
// const markHeight = 78
// const markWeight = 1.69
// const jonhHeight = 92
// const jonhWeight = 1.76

// const markBmi = markWeight / markHeight ** 2
// const jonhBmi = jonhWeight / jonhHeight ** 2
// let markHigherBMI = markBmi > jonhBmi
// console.log(markBmi + '\n', jonhBmi + '\n', markHigherBMI)

// END - Coding Challenge #1

let Fundamental_1_Coding_Challenge_2
// Coding Challenge #2
// Use the BMI example from Challenge #1, and the code you already wrote, and
// improve it.
// Your tasks:
// 1. Print a nice output to the console, saying who has the higher BMI. The message
// is either "Mark's BMI is higher than John's!" or "John's BMI is higher than Mark's!"
// 2. Use a template literal to include the BMI values in the outputs. Example: "Mark's
// BMI (28.3) is higher than John's (23.9)!"

// ------------SOLUTION------------
// if (markHigherBMI) {
//   console.log(`Mark's BMI (${markBmi}) is higher than John's (${jonhBmi})!`)
// } else {
//   console.log(`Jonh's BMI (${jonhBmi}) is higher than Mark's (${markBmi})!`)
// }
// END -Coding Challenge #2

let Fundamental_1_Coding_Challenge_3
// Coding Challenge #3
// There are two gymnastics teams, Dolphins and Koalas. They compete against each
// other 3 times. The winner with the highest average score wins a trophy!
// Your tasks:
// 1. Calculate the average score for each team, using the test data below
// 2. Compare the team's average scores to determine the winner of the competition,
// and print it to the console. Don't forget that there can be a draw, so test for that
// as well (draw means they have the same average score)
// 3. Bonus 1: Include a requirement for a minimum score of 100. With this rule, a
// team only wins if it has a higher score than the other team, and the same time a
// score of at least 100 points. Hint: Use a logical operator to test for minimum
// score, as well as multiple else-if blocks 😉
// 4. Bonus 2: Minimum score also applies to a draw! So a draw only happens when
// both teams have the same score and both have a score greater or equal 100
// points. Otherwise, no team wins the trophy
// Test data:
// § Data 1: Dolphins score 96, 108 and 89. Koalas score 88, 91 and 110
// § Data Bonus 1: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 123
// § Data Bonus 2: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 106

// ------------SOLUTION------------

// const averagDolphines = (96 + 108 + 89) / 3
// const averagKolas = (88 + 91 + 110) / 3
// console.log(averagDolphines, averagKolas)

// if (averagDolphines > averagKolas) {
//   console.log(`Dolphines is the Winner🏆🏆`)
// } else if (averagDolphines > averagKolas) {
//   console.log(`Kolas is the Winner🏆🏆`)
// } else if (averagDolphines === averagKolas) {
//   console.log(`Both Are  the Winner😎`)
// } else {
//   console.log(`No One wins😉`)
// }
//END - Coding Challenge #3

let Fundamental_1_Coding_Challenge_4
// Coding Challenge #4
// Steven wants to build a very simple tip calculator for whenever he goes eating in a
// restaurant. In his country, it's usual to tip 15% if the bill value is between 50 and
// 300. If the value is different, the tip is 20%.
// Your tasks:
// 1. Calculate the tip, depending on the bill value. Create a variable called 'tip' for
// this. It's not allowed to use an if/else statement 😅 (If it's easier for you, you can
// start with an if/else statement, and then try to convert it to a ternary
// operator!)
// 2. Print a string to the console containing the bill value, the tip, and the final value
// (bill + tip). Example: “The bill was 275, the tip was 41.25, and the total value
// 316.25”
// Test data:
// § Data 1: Test for bill values 275, 40 and 430
// Hints:
// § To calculate 20% of a value, simply multiply it by 20/100 = 0.2
// § Value X is between 50 and 300, if it's >= 50 && <= 300 😉

// const bill = 150
// const tip = (bill * (bill >= 50 && bill <= 300 ? 15 : 20)) / 100

// console.log(
//   `The bill was ${bill}, the tip was ${tip}, and the total value ${
//     bill + tip
//   }`
// )

// END - Coding Challenge #4

let Fundamental_2_Coding_Challenge_5
//Functions Coding_Challenge_5
// 1. Create an arrow function 'calcAverage' to calculate the average of 3 scores
// 2. Use the function to calculate the average for both teams
// 3. Create a function 'checkWinner' that takes the average score of each team
// as parameters ('avgDolhins' and 'avgKoalas'), and then logs the winner
// to the console, together with the victory points, according to the rule above.
// Example: "Koalas win (30 vs. 13)"
// 4. Use the 'checkWinner' function to determine the winner for both Data 1 and
// Data 2
// 5. Ignore draws this time
// Test data:
// § Data 1: Dolphins score 44, 23 and 71. Koalas score 65, 54 and 49
// § Data 2: Dolphins score 85, 54 and 41. Koalas score 23, 34 and 27
// Hints:
// § To calculate average of 3 values, add them all together and divide by 3
// § To check if number A is at least double number B, check for A >= 2 * B.

// const calcAverage = (total, noOfAttempts) => total / noOfAttempts
// function checkWinner(totalDolphines, totalKoalas) {
//   const avgDolphines = calcAverage(totalDolphines, 3)
//   const avgKoalas = calcAverage(totalKoalas, 3)
//   if (avgDolphines >= avgKoalas * 2) {
//     console.log(`Dolphines wins (${avgDolphines}vs${avgKoalas})🏆`)
//   } else if (avgKoalas >= avgDolphines * 2) {
//     console.log(`Koalas wins (${avgKoalas} vs ${avgDolphines})🏆`)
//   } else {
//     console.log('No One Wins!')
//   }
// }
// checkWinner(44 + 23 + 71, 65 + 54 + 49)

//END - Functions Coding_Challenge_5

let Fundamental_2_Coding_Challenge_6
//Arrays Coding_Challenge_6
// Steven is still building his tip calculator, using the same rules as before: Tip 15% of
// the bill if the bill value is between 50 and 300, and if the value is different, the tip is
// 20%.
// Your tasks:
// 1. Write a function 'calcTip' that takes any bill value as an input and returns
// the corresponding tip, calculated based on the rules above (you can check out
// the code from first tip calculator challenge if you need to). Use the function
// type you like the most. Test the function using a bill value of 100
// 2. And now let's use arrays! So create an array 'bills' containing the test data
// below
// 3. Create an array 'tips' containing the tip value for each bill, calculated from
// the function you created before
// 4. Bonus: Create an array 'total' containing the total values, so the bill + tip
// Test data: 125, 555 and 44
// Hint: Remember that an array needs a value in each position, and that value can
// actually be the returned value of a function! So you can just call a function as array
// values (so don't store the tip values in separate variables first, but right in the new
// array) �

// const bills = [125, 555, 44]
// const calcTip = (bill) => {
//   const tip = (bill * (bill >= 50 && bill <= 300 ? 15 : 20)) / 100
//   return tip
// }
// function showTip(bill, tip) {
//   console.log(`The bill was ${bill}, the tip was ${tip}, and the total value
//   ${bill + tip}`)
// }
// showTip(bills[0], calcTip(bills[0]))
// showTip(bills[1], calcTip(bills[1]))
// showTip(bills[bills.length - 1], calcTip(bills[bills.length - 1]))

//END - Arrays Coding_Challenge_5

let Fundamental_2_Coding_Challenge_7
// Coding Challenge #3
// Let's go back to Mark and John comparing their BMIs! This time, let's use objects to
// implement the calculations! Remember: BMI = mass / height ** 2 = mass
// / (height * height) (mass in kg and height in meter)
// Your tasks:
// 1. For each of them, create an object with properties for their full name, mass, and
// height (Mark Miller and John Smith)
// 2. Create a 'calcBMI' method on each object to calculate the BMI (the same
// method on both objects). Store the BMI value to a property, and also return it
// from the method
// 3. Log to the console who has the higher BMI, together with the full name and the
// respective BMI. Example: "John's BMI (28.3) is higher than Mark's (23.9)!"
// Test data: Marks weights 78 kg and is 1.69 m tall. John weights 92 kg and is 1.95 m

// const mark = {
//   fullName: 'Mark Miller',
//   mass: 90,
//   height: 1.95,
//   calcBMI: function () {
//     this.BMI = this.mass / this.height ** 2
//     return this.BMI
//   },
// }
// const jonh = {
//   fullName: 'John Smith',
//   mass: 92,
//   height: 1.95,
//   calcBMI: function () {
//     this.BMI = this.mass / (this.height * this.height)
//     return this.BMI
//   },
// }
// mark.calcBMI()
// jonh.calcBMI()
// console.log(mark.BMI, jonh.BMI)
// const result =
//   mark.BMI > jonh.BMI
//     ? `${mark.fullName}'s BMI (${mark.BMI}) is higher than ${jonh.fullName}'s (${jonh.BMI})!`
//     : `${jonh.fullName}'s BMI (${jonh.BMI}) is higher than ${mark.fullName}'s (${mark.BMI})!`
// console.log(result)

// END - Coding Challenge #3

let Fundamental_2_Coding_Challenge_8
// Loops Coding Challenge #4
// Let's improve Steven's tip calculator even more, this time using loops!
// Your tasks:
// 1. Create an array 'bills' containing all 10 test bill values
// 2. Create empty arrays for the tips and the totals ('tips' and 'totals')
// 3. Use the 'calcTip' function we wrote before (no need to repeat) to calculate
// tips and total values (bill + tip) for every bill value in the bills array. Use a for
// loop to perform the 10 calculations!
// Test data: 22, 295, 176, 440, 37, 105, 10, 1100, 86 and 52
// Hints: Call ‘calcTip ‘in the loop and use the push method to add values to the
// tips and totals arrays �
// Bonus:
// 4. Bonus: Write a function 'calcAverage' which takes an array called 'arr' as
// an argument. This function calculates the average of all numbers in the given
// array. This is a difficult challenge (we haven't done this before)! Here is how to
// solve it:
// 4.1. First, you will need to add up all values in the array. To do the addition,
// start by creating a variable 'sum' that starts at 0. Then loop over the
// array using a for loop. In each iteration, add the current value to the
// 'sum' variable. This way, by the end of the loop, you have all values
// added together
// 4.2. To calculate the average, divide the sum you calculated before by the
// length of the array (because that's the number of elements)
// 4.3. Call the function with the 'totals' array

// const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52]
// let tips = []
// let totals = []
// const calcTip = (bill) => {
//   const tip = (bill * (bill >= 50 && bill <= 300 ? 15 : 20)) / 100
//   return tip
// }
// for (let i = 0; i <= bills.length - 1; i++) {
//   tips.push(calcTip(bills[i]))
//   totals.push(bills[i] + tips[i])
// }
// // console.log(tips)
// // console.log(totals)
// function calcAverage(arr) {
//   let total = 0
//   for (let i = 0; i <= arr.length - 1; i++) {
//     total = total + arr[i]
//   }
//   return total / arr.length
// }
// const arrss = [10, 20, 50, 40, 60, 90]
// console.log(calcAverage(arrss))

//END -  Loops Coding Challenge #4

let Fundamental_2_DataStructures_Modern_Operators_1
// 1. Create one player array for each team (variables 'players1' and
// 'players2')
// 2. The first player in any player array is the goalkeeper and the others are field
// players. For Bayern Munich (team 1) create one variable ('gk') with the
// goalkeeper's name, and one array ('fieldPlayers') with all the remaining 10
// field players
// 3. Create an array 'allPlayers' containing all players of both teams (22
// players)
// 4. During the game, Bayern Munich (team 1) used 3 substitute players. So create a
// new array ('players1Final') containing all the original team1 players plus
// 'Thiago', 'Coutinho' and 'Perisic'
// 5. Based on the game.odds object, create one variable for each odd (called
// 'team1', 'draw' and 'team2')
// 6. Write a function ('printGoals') that receives an arbitrary number of player
// names (not an array) and prints each of them to the console, along with the
// number of goals that were scored in total (number of player names passed in)
// 7. The team with the lower odd is more likely to win. Print to the console which
// team is more likely to win, without using an if/else statement or the ternary
// operator.
// Test data for 6.: First, use players 'Davies', 'Muller', 'Lewandowski' and 'Kimmich'.
// Then, call the function again with players from game.scored

//Data from Web
// const game = {
//   team1: 'Bayern Munich',
//   team2: 'Borrussia Dortmund',
//   players: [
//     [
//       'Neuer',
//       'Pavard',
//       'Martinez',
//       'Alaba',
//       'Davies',
//       'Kimmich',
//       'Goretzka',
//       'Coman',
//       'Muller',
//       'Gnarby',
//       'Lewandowski',
//     ],
//     [
//       'Burki',
//       'Schulz',
//       'Hummels',
//       'Akanji',
//       'Hakimi',
//       'Weigl',
//       'Witsel',
//       'Hazard',
//       'Brandt',
//       'Sancho',
//       'Gotze',
//     ],
//   ],
//   score: '4:0',
//   scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
//   date: 'Nov 9th, 2037',
//   odds: {
//     team1: 1.33,
//     x: 3.25,
//     team2: 6.5,
//   },
// }
// 1) seperating players into player1 and player2 using destructuring
// const [players1, players2] = game.players
// console.log(players1, players2)

// 2)first player as GK nd rest as array using Rest pattern and paramtere destructuring
// const [gk1, ...otherplayers1] = players1
// console.log(gk1, otherplayers1)
// const [gk2, ...otherplayers2] = players2
// console.log(gk2, otherplayers2)

// 3)All players using spread
// const AllPlayers = [...players1, ...players2]
// console.log(AllPlayers)

// 4)Final Players
// const FinalPlayers = [...players1, 'Thiago', 'Coutinho', 'Perisic']
// console.log(FinalPlayers)

// 5)destructrng Odds object
// const {
//   odds: { team1, x: draw, team2 },
// } = game
// console.log(team1, draw, team2)
// 6)goals wer score acc to players passed in function using rest parameters

// const PrintGoals = function (...players) {
//   console.log(`${players.length} goals were scored!!`)
// }
// PrintGoals('Davies', 'Muller', 'Lewandowski', 'Kimmich')
// PrintGoals('Davies', 'Kimmich')
// PrintGoals(game.scored)
// PrintGoals(...game.scored)

// 7) print team wins
// team1 > team2 && console.log('Team1 wins')
// team1 < team2 && console.log('Team2 wins')

let Fundamental_2_DataStructures_Modern_Operators_2
// 1. Loop over the game.scored array and print each player name to the console,
// along with the goal number (Example: "Goal 1: Lewandowski")
// 2. Use a loop to calculate the average odd and log it to the console (We already
// studied how to calculate averages, you can go check if you don't remember)
// 3. Print the 3 odds to the console, but in a nice formatted way, exactly like this:
// Odd of victory Bayern Munich: 1.33
// Odd of draw: 3.25
// Odd of victory Borrussia Dortmund: 6.5
// Get the team names directly from the game object, don't hardcode them
// (except for "draw"). Hint: Note how the odds and the game objects have the
// same property names �
// 4. Bonus: Create an object called 'scorers' which contains the names of the
// players who scored as properties, and the number of goals as the value. In this
// game, it will look like this:
// {
//  Gnarby: 1,
//  Hummels: 1,
//  Lewandowski: 2
// }
// const game = {
//   team1: 'Bayern Munich',
//   team2: 'Borrussia Dortmund',
//   players: [
//     [
//       'Neuer',
//       'Pavard',
//       'Martinez',
//       'Alaba',
//       'Davies',
//       'Kimmich',
//       'Goretzka',
//       'Coman',
//       'Muller',
//       'Gnarby',
//       'Lewandowski',
//     ],
//     [
//       'Burki',
//       'Schulz',
//       'Hummels',
//       'Akanji',
//       'Hakimi',
//       'Weigl',
//       'Witsel',
//       'Hazard',
//       'Brandt',
//       'Sancho',
//       'Gotze',
//     ],
//   ],
//   score: '4:0',
//   scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
//   date: 'Nov 9th, 2037',
//   odds: {
//     team1: 1.33,
//     x: 3.25,
//     team2: 6.5,
//   },
// }

//1) Loop through scroed array
// for (const score of game.scored.entries()) {
//   console.log(`Goal ${score[0]} :${score[1]}`)
// }
// OR
// for (const [i, el] of game.scored.entries()) {
//   console.log(`Goal ${i} :${el}`)
// }

//2)Use Loop to calculate Odds
// const odds = Object.values(game.odds)
// // console.log(odds)
// let average = 0
// for (const odd of odds) {
//   average += odd
// }
// average = average / odds.length
// console.log(average)

//3)Print odds of 3 with name in format
// Odd of victory Bayern Munich: 1.33
// Odd of draw: 3.25
// Odd of victory Borrussia Dortmund: 6.5
// const odds = Object.entries(game.odds)
// for (const [team, odd] of odds) {
//   const str = team === 'x' ? 'draw' : `victory ${game[team]}`
//   console.log(`Odd of ${str} ${odd}`)
// }

// 4)Bonus
// const scorers = {}
// for (const player of game.scored) {
//   scorers[player] ? scorers[player]++ : (scorers[player] = 1)
// }
// console.log(scorers)

let Fundamental_2_DataStructures_Modern_Operators_3
// Let's continue with our football betting app! This time, we have a map called
// 'gameEvents' (see below) with a log of the events that happened during the
// game. The values are the events themselves, and the keys are the minutes in which
// each event happened (a football game has 90 minutes plus some extra time).
// Your tasks:
// 1. Create an array 'events' of the different game events that happened (no
// duplicates)
// 2. After the game has finished, is was found that the yellow card from minute 64
// was unfair. So remove this event from the game events log.
// 3. Compute and log the following string to the console: "An event happened, on
// average, every 9 minutes" (keep in mind that a game has 90 minutes)
// 4. Loop over 'gameEvents' and log each element to the console, marking
// whether it's in the first half or second half (after 45 min) of the game, like this:
// [FIRST HALF] 17: ⚽ GOAL
// const gameEvents = new Map([
//   [17, '⚽ GOAL'],
//   [36, '� Substitution'],
//   [47, '⚽ GOAL'],
//   [61, '� Substitution'],
//   [64, '� Yellow card'],
//   [69, '� Red card'],
//   [70, '� Substitution'],
//   [72, '� Substitution'],
//   [76, '⚽ GOAL'],
//   [80, '⚽ GOAL'],
//   [92, '� Yellow card'],
// ])
// console.log(gameEvents)
//1) creatae event  array of unique values
// console.log(gameEvents.values())
// const events = [...new Set(gameEvents.values())]
// console.log(events)

// 2)Delete and event
// gameEvents.delete(64)
// console.log(gameEvents)

//3) write string with total time /events
// const time = [...gameEvents.keys()].pop() //gives last ele back
// console.log(time)
// console.log(
//   `An event happened, on average, every ${time / gameEvents.size} Minutes`
// )

//Loop over 'gameEvents' and log each element to the console[FIRST HALF] 17: ⚽ GOAL
// for (const [min, event] of gameEvents) {
//   const half = min <= 45 ? 'FIRST' : 'SECOND'
//   console.log(`[${half} HALF] ${min} : ${event} `)
// }

let Fundamental_2_DataStructures_Working_With_Strings_1
// const btnEL = document.getElementById('btnsubmit')
// const names = document.querySelector('textarea')
// btnEL.addEventListener('click', function () {
//   let text = names.value
//   text = text.split('\n')

//   1) Udemy Trick
//   for (let [i, name] of text.entries()) {
//     const [first, second] = name.trim().toLowerCase().split('_')
//     name = `${first}${second.replace(second[0], second[0].toUpperCase())}`
//     name = name.padEnd(25) + '✔'.repeat(i + 1)
//     console.log(name)
//   }

//   2)  using my trick logic
//   let cnt = 0
//   for (let name of text) {
//     cnt++
//     name = name.trim()
//     name = name.split('_')
//     name = name[0] + name[1][0].toUpperCase() + name[1].slice(1)
//     const tick = ''.padStart(cnt, '✔')
//     name = name + ` ${tick}`
//     console.log(name)
//   }

//   3) using my trick  substring method
//   let cnt = 0
//   for (let name of text) {
//     cnt++
//     name =
//       name.trim().substring(0, name.indexOf('_')) +
//       name[name.indexOf('_') + 1].toUpperCase() +
//       name.substring(name.indexOf('_') + 2)
//     name = name.padEnd(20) + '🛠'.repeat(cnt)
//     console.log(name)
//   }
// })

let Fundamental_2_DataStructures_Working_With_Strings_2
// let flights = `_Delayed_Departure;fao93766109;txl2133758440;11:25+
//   _Arrival;bru0943384722;fao93766109;11:45+
//   _Delayed_Arrival;hel7439299980;fao93766109;12:05+
//   _Departure;fao93766109;lis2323639855;12:30`

//my trick
// for (let [i, row] of flights.split('+').entries()) {
//   let [sts, from, to, time] = row.split(';')
//   sts = sts.split('_').join(' ').trim()
//   sts = sts.startsWith('Delayed') ? `🔴${sts}` : `${sts}`
//   from = from.toUpperCase().slice(0, 3)
//   to = to.toUpperCase().slice(0, 3)
//   time = time.replace(':', 'h')
//   const result =
//     `${i + 1})` + `${sts} from ${from} to ${to} (${time})`.padStart(50)
//   console.log(result)
// }

//Udemy trick
// for (let row of flights.split('+')) {
//   let [sts, from, to, time] = row.split(';')
//   const result = `${sts.trim().startsWith('_Delayed') ? '🔴' : ''} ${sts
//     .replaceAll('_', ' ')
//     .trim()} from ${from.slice(0, 3).toUpperCase()} to ${to
//     .slice(0, 3)
//     .toUpperCase()} (${time.replace(':', 'h')})`.padStart(50)
//   console.log(result)
// }

let Fundamental_2_Functions_1
// 1.1. Display a prompt window for the user to input the number of the
// selected option. The prompt should look like this:
// What is your favourite programming language?
// 0: JavaScript
// 1: Python
// 2: Rust
// 3: C++
// (Write option number)
// 1.2. Based on the input number, update the 'answers' array property. For
// example, if the option is 3, increase the value at position 3 of the array by
// 1. Make sure to check if the input is a number and if the number makes
// sense (e.g. answer 52 wouldn't make sense, right?)
// 2. Call this method whenever the user clicks the "Answer poll" button.
// 3. Create a method 'displayResults' which displays the poll results. The
// method takes a string as an input (called 'type'), which can be either 'string'
// or 'array'. If type is 'array', simply display the results array as it is, using
// console.log(). This should be the default option. If type is 'string', display a
// string like "Poll results are 13, 2, 4, 1".
// 4. Run the 'displayResults' method at the end of each
// 'registerNewAnswer' method call.
// 5. Bonus: Use the 'displayResults' method to display the 2 arrays in the test
// data. Use both the 'array' and the 'string' option. Do not put the arrays in the poll
// object! So what should the this keyword look like in this situation?
// The Complete JavaScript Course 21
// Test data for bonus:
// § Data 1: [5, 2, 3]
// § Data 2: [1, 5, 3, 9, 6, 1]
// Hints: Use many of the tools you learned about in this and the last section

// const poll = {
//   question: 'What is your favourite programming language?',
//   options: ['0: JavaScript', '1: Python', '2: Rust', '3:C++'],
//   // This generates [0, 0, 0, 0]. More in the next section!
//   answers: new Array(4).fill(0),
//   registerNewAnswer() {
//     //Get Answer
//     const answer = Number(
//       prompt(`${this.question}\n${this.options.join('\n')}
//     \n(Write Option number)`)
//     )
//     console.log(answer)

//     //Register
//     typeof answer === 'number' &&
//       answer < this.options.length &&
//       this.answers[answer]++
//     console.log(this.answers)
//     this.displayResult('array')
//     this.displayResult('string')
//   },

//   displayResult(type = 'array') {
//     if (type === 'array') {
//       console.log(this.answers)
//     } else if (type === 'string') {
//       console.log(`Polls are  ${[...this.answers]}`)
//     }
//   },
// }
// document
//   .querySelector('.poll')
//   .addEventListener('click', poll.registerNewAnswer.bind(poll))

let Fundamental_2_Functions_2
// 1. Take the IIFE below and at the end of the function, attach an event listener that
// changes the color of the selected h1 element ('header') to blue, each time
// the body element is clicked. Do not select the h1 element again!
// 2. And now explain to yourself (or someone around you) why this worked! Take all
// the time you need. Think about when exactly the callback function is executed,
// and what that means for the variables involved in this example

// ;(function () {
//   const header = document.querySelector('h1')
//   header.style.color = 'red'

//   document.body.addEventListener('click', function () {
//     header.style.color = 'blue'
//   })
// })()

let Working_with_Arrays_1
// Julia and Kate are doing a study on dogs. So each of them asked 5 dog owners
// about their dog's age, and stored the data into an array (one array for each). For
// now, they are just interested in knowing whether a dog is an adult or a puppy.
// A dog is an adult if it is at least 3 years old, and it's a puppy if it's less than 3 years
// old.
// Your tasks:
// Create a function 'checkDogs', which accepts 2 arrays of dog's ages
// ('dogsJulia' and 'dogsKate'), and does the following things:
// 1. Julia found out that the owners of the first and the last two dogs actually have
// cats, not dogs! So create a shallow copy of Julia's array, and remove the cat
// ages from that copied array (because it's a bad practice to mutate function
// parameters)
// 2. Create an array with both Julia's (corrected) and Kate's data
// 3. For each remaining dog, log to the console whether it's an adult ("Dog number 1
// is an adult, and is 5 years old") or a puppy ("Dog number 2 is still a puppy
// �
// ")
// 4. Run the function for both test datasets
// Test data:
// § Data 1: Julia's data [3, 5, 2, 12, 7], Kate's data [4, 1, 15, 8, 3]
// § Data 2: Julia's data [9, 16, 6, 8, 3], Kate's data [10, 5, 6, 1, 4]

// const checkDogs = function (dogsJulia, dogsKate) {
//   //1) remove 1 and last two elemsts(cats) from julia original array
//   const juliaDogsNew = dogsJulia.slice(1, 3)

//   //2)combine both arrays
//   const combinedDogs = [...juliaDogsNew, ...dogsKate]

//   //   const combinedDogs = juliaDogsNew.concat(dogsKate)
//   console.log(...combinedDogs)

//   //3)Foreach and console
//   combinedDogs.forEach((dog, index) => {
//     if (dog >= 3) {
//       console.log(
//         `Dog number ${index + 1} is an adult, and is ${dog} years old`
//       )
//     } else {
//       console.log(`Dog number ${index + 1} is still a puppy`)
//     }
//   })
// }

// checkDogs([3, 5, 2, 12, 7], [4, 1, 15, 8, 3])
// checkDogs([9, 16, 6, 8, 3], [10, 5, 6, 1, 4])

let Working_with_Arrays_2
// Let's go back to Julia and Kate's study about dogs. This time, they want to convert
// dog ages to human ages and calculate the average age of the dogs in their study.
// Your tasks:
// Create a function 'calcAverageHumanAge', which accepts an arrays of dog's
// ages ('ages'), and does the following things in order:
// 1. Calculate the dog age in human years using the following formula: if the dog is
// <= 2 years old, humanAge = 2 * dogAge. If the dog is > 2 years old,
// humanAge = 16 + dogAge * 4
// 2. Exclude all dogs that are less than 18 human years old (which is the same as
// keeping dogs that are at least 18 years old)
// 3. Calculate the average human age of all adult dogs (you should already know
// from other challenges how we calculate averages �)
// 4. Run the function for both test datasets
// Test data:
// § Data 1: [5, 2, 4, 1, 15, 8, 3]
// § Data 2: [16, 6, 10, 5, 6, 1, 4]

// const calcAverageHumanAge = function (ages) {
//   const humanAges = ages.map((age, index) => {
//     if (age <= 2) {
//       return 2 * age
//     } else {
//       return 16 + age * 4
//     }
//   })
//   console.log('humanAges', humanAges)
//   const above18 = humanAges.filter((age) => age >= 18)
//   console.log('above18', above18)

//   const averageAdults =
//     above18.reduce((acc, curr) => acc + curr, 0) / above18.length
//   //   console.log(sumAdults / above18.length)
//   console.log(' averageAdults', averageAdults)
//   return averageAdults
// }

//Using chaining and Lamba expression
// const calcAverageHumanAge = (ages) => {
//   return ages
//     .map((age) => {
//       if (age <= 2) {
//         return 2 * age
//       } else {
//         return 16 + age * 4
//       }
//     })
//     .filter((age) => {
//       return age >= 18
//     })
//     .reduce((acc, curr, i, arr) => {
//       return acc + curr / arr.length
//     }, 0)
// }
// const avg1 = calcAverageHumanAge([5, 2, 4, 1, 15, 8, 3])
// const avg2 = calcAverageHumanAge([16, 6, 10, 5, 6, 1, 4])
// console.log(avg1, avg2) //44 ,47.333333333333336

let Working_with_Arrays_3
// Julia and Kate are still studying dogs, and this time they are studying if dogs are
// eating too much or too little.
// Eating too much means the dog's current food portion is larger than the
// recommended portion, and eating too little is the opposite.
// Eating an okay amount means the dog's current food portion is within a range 10%
// above and 10% below the recommended portion (see hint).
// Your tasks:
// 1. Loop over the 'dogs' array containing dog objects, and for each dog, calculate
// the recommended food portion and add it to the object as a new property. Do
// not create a new array, simply loop over the array. Forumla:
// recommendedFood = weight ** 0.75 * 28. (The result is in grams of
// food, and the weight needs to be in kg)
// 2. Find Sarah's dog and log to the console whether it's eating too much or too
// little. Hint: Some dogs have multiple owners, so you first need to find Sarah in
// the owners array, and so this one is a bit tricky (on purpose) �
// 3. Create an array containing all owners of dogs who eat too much
// ('ownersEatTooMuch') and an array with all owners of dogs who eat too little
// ('ownersEatTooLittle').
// 4. Log a string to the console for each array created in 3., like this: "Matilda and
// Alice and Bob's dogs eat too much!" and "Sarah and John and Michael's dogs eat
// too little!"
// 5. Log to the console whether there is any dog eating exactly the amount of food
// that is recommended (just true or false)
// 6. Log to the console whether there is any dog eating an okay amount of food
// (just true or false)
// 7. Create an array containing the dogs that are eating an okay amount of food (try
// to reuse the condition used in 6.)
// 8. Create a shallow copy of the 'dogs' array and sort it by recommended food
// portion in an ascending order (keep in mind that the portions are inside the
// array's objects �)
// Hints:
// § Use many different tools to solve these challenges, you can use the summary
// lecture to choose between them �
// § Being within a range 10% above and below the recommended portion means:
// current > (recommended * 0.90) && current < (recommended *
// 1.10). Basically, the current portion should be between 90% and 110% of the
// recommended portion
// const dogs = [
//   { weight: 22, curFood: 250, owners: ['Alice', 'Bob'] },
//   { weight: 8, curFood: 200, owners: ['Matilda'] },
//   { weight: 13, curFood: 275, owners: ['Sarah', 'John'] },
//   { weight: 32, curFood: 340, owners: ['Michael'] },
// ]

//1)
// dogs.map((dog) => {
//   dog.recFood = Math.trunc(dog.weight ** 0.75 * 28)
// })
// console.log(dogs)

// 2)
// const sarahDog = dogs.find((dog) => dog.owners.includes('Sarah'))
// console.log(
//   sarahDog.curFood > sarahDog.recFood
//     ? "Sarah's Dog is Eating too Much"
//     : "Sarah's Dog is Eating Less"
// )

//3)
// const ownersEatTooMuch = dogs
//   .filter((dog) => dog.recFood > dog.curFood)
//   .flatMap((o) => o.owners)
// const ownersEatTooLittle = dogs
//   .filter((dog) => dog.recFood < dog.curFood)
//   .flatMap((o) => o.owners)
// console.log('ownersEatTooMuch : ', ownersEatTooMuch)
// console.log('ownersEatTooLittle : ', ownersEatTooLittle)

// 4) use 3)
// console.log(`${ownersEatTooMuch.join(' and ')}\'s dogs eat too much!`)
// console.log(`${ownersEatTooLittle.join(' and ')}\'s dogs eat too little!`)

//5)
// console.log(dogs.some((dog) => dog.curFood === dog.recFood))

// 6)
// console.log(
//   dogs.some(
//     (dog) => dog.curFood > dog.recFood * 0.9 && dog.curFood < dog.recFood * 1.1
//   )
// )

// 7)
// const dogsRe = dogs.filter(
//   (dog) => dog.curFood > dog.recFood * 0.9 && dog.curFood < dog.recFood * 1.1
// )
// console.log(dogsRe)

//8)
// const sortedDogs = dogs.slice().sort((a, b) => a.recFood - b.recFood)
// console.log(dogs.map((dog) => dog.recFood))
// console.log(sortedDogs)

let Object_Oriented_Programming_1
// 1. Use a constructor function to implement a 'Car'. A car has a 'make' and a
// 'speed' property. The 'speed' property is the current speed of the car in
// km/h
// 2. Implement an 'accelerate' method that will increase the car's speed by 10,
// and log the new speed to the console
// 3. Implement a 'brake' method that will decrease the car's speed by 5, and log
// the new speed to the console
// 4. Create 2 'Car' objects and experiment with calling 'accelerate' and
// 'brake' multiple times on each of them
// Test data:
// § Data car 1: 'BMW' going at 120 km/h
// § Data car 2: 'Mercedes' going at 95 km/h

// const Car = function (make, speed) {
//   this.make = make
//   this.speed = speed
// }
// const bmw = new Car('BMW', 120)
// const mercedes = new Car('Mercedes', 95)
// console.log(bmw, mercedes)
// Car.prototype.accelerate = function () {
//   this.speed += 10
//   console.log(`${this.make} accelerate by 10 : ${this.speed}`)
// }
// Car.prototype.brake = function () {
//   this.speed -= 5
//   console.log(`${this.make} Brake by 5 : ${this.speed}`)
// }
// const play = () => {
//   bmw.accelerate()
//   bmw.accelerate()
//   bmw.accelerate()
//   bmw.brake()
//   bmw.brake()
//   bmw.brake()
//   bmw.brake()
//   bmw.accelerate()
//   bmw.accelerate()
//   mercedes.brake()
//   mercedes.accelerate()
//   mercedes.brake()
//   mercedes.brake()
//   mercedes.accelerate()
//   mercedes.accelerate()
// }
// play()
// console.log(bmw, mercedes)

let Object_Oriented_Programming_2
// 1. Re-create Challenge #1, but this time using an ES6 class (call it 'CarCl')
// 2. Add a getter called 'speedUS' which returns the current speed in mi/h (divide
// by 1.6)
// 3. Add a setter called 'speedUS' which sets the current speed in mi/h (but
// converts it to km/h before storing the value, by multiplying the input by 1.6)
// 4. Create a new car and experiment with the 'accelerate' and 'brake'
// methods, and with the getter and setter.
// Test data:
// § Data car 1: 'Ford' going at 120 km/

// class CarCl {
//   constructor(make, speed) {
//     this.make = make
//     this.speed = speed //km/hr
//   }

//   get speedUS() {
//     return this.speed / 1.6 //m/h
//   }
//   set speedUS(speed) {
//     this.speed = speed * 1.6
//   }

//   accelerate() {
//     this.speed += 10
//     console.log(`${this.make} accelerate by 10 : ${this.speed}`)
//   }
//   brake() {
//     this.speed -= 5
//     console.log(`${this.make} Brake by 5 : ${this.speed}`)
//   }
// }
// const ford = new CarCl('Ford', 120)
// ford.accelerate()
// ford.accelerate()
// ford.brake()
// ford.accelerate()
// ford.brake()
// ford.brake()
// ford.accelerate()
// ford.accelerate()
// ford.accelerate()
// console.log(ford)

let Object_Oriented_Programming_3
// 1. Use a constructor function to implement an Electric Car (called 'EV') as a child
// "class" of 'Car'. Besides a make and current speed, the 'EV' also has the
// current battery charge in % ('charge' property)
// 2. Implement a 'chargeBattery' method which takes an argument
// 'chargeTo' and sets the battery charge to 'chargeTo'
// 3. Implement an 'accelerate' method that will increase the car's speed by 20,
// and decrease the charge by 1%. Then log a message like this: 'Tesla going at 140
// km/h, with a charge of 22%'
// 4. Create an electric car object and experiment with calling 'accelerate',
// 'brake' and 'chargeBattery' (charge to 90%). Notice what happens when
// you 'accelerate'! Hint: Review the definiton of polymorphism �
// Test data:
// § Data car 1: 'Tesla' going at 120 km/h, with a charge of 23%
// #region Car Constructr Class
// const Car = function (make, speed) {
//   this.make = make
//   this.speed = speed
// }

// Car.prototype.accelerate = function () {
//   this.speed += 10
//   console.log(`${this.make} accelerate by 10 : ${this.speed}`)
// }
// Car.prototype.brake = function () {
//   this.speed -= 5
//   console.log(`${this.make} Brake by 5 : ${this.speed}`)
// }

// #endregion
// #region EV Constructor Class

// const EV = function (make, speed, charge) {
//     this.make = make
//     this.speed = speed
//   Car.call(this, make, speed)
//   this.charge = charge //in %
// }

// Linking Object
// EV.prototype = Object.create(Car.prototype)

// EV.prototype.chargeBattery = function (chargeTo) {
//   this.charge = chargeTo
// }
// EV.prototype.accelerate = function () {
//   this.speed += 20
//   this.charge -= 1
//   console.log(
//     `${this.make} going at ${this.speed} km/h, with a charge of ${this.charge}%'`
//   )
// }
// EV.prototype.brake = function () {
//   this.speed -= 5
//   console.log(
//     `${this.make} going at ${this.speed} km/h, with a charge of ${this.charge}%'`
//   )
// }
// #endregion
// const Tesla = new EV('Tesla', 120, 23)
// Tesla.chargeBattery(90)
// Tesla.accelerate()
// Tesla.accelerate()
// Tesla.accelerate()
// Tesla.brake()
// console.log(Tesla)

let Object_Oriented_Programming_4
// 1. Re-create Challenge #3, but this time using ES6 classes: create an 'EVCl'
// child class of the 'CarCl' class
// 2. Make the 'charge' property private
// 3. Implement the ability to chain the 'accelerate' and 'chargeBattery'
// methods of this class, and also update the 'brake' method in the 'CarCl'
// class. Then experiment with chaining!
// Test data:
// § Data car 1: 'Rivian' going at 120 km/h, with a charge of 23%

// class CarCl {
//   constructor(make, speed) {
//     this.make = make
//     this.speed = speed
//   }

//   accelerate() {
//     this.speed += 10
//     console.log(`${this.make} accelerate by 10 : ${this.speed}`)
//   }
//   brake() {
//     this.speed -= 5
//     console.log(`${this.make} Brake by 5 : ${this.speed}`)
//   }
// }

// class EVCl extends CarCl {
//   #charge
//   constructor(make, speed, charge) {
//     super(make, speed)
//     this.#charge = charge
//   }
//   accelerate() {
//     this.speed += 20
//     this.#charge--
//     console.log(
//       `${this.make} going at ${this.speed} km/h, with a charge of ${
//         this.#charge
//       }%'`
//     )
//     return this
//   }
//   brake() {
//     this.speed -= 5
//     console.log(
//       `${this.make} going at ${this.speed} km/h, with a charge of ${
//         this.#charge
//       }%'`
//     )
//     return this
//   }
// }

// const Tesla = new EVCl('Tesla', 120, 50)
// console.log(Tesla)

// Tesla.accelerate().brake().accelerate().accelerate().brake().brake()
// console.log(Tesla)

let Asynchronous_API_1
// In this challenge you will build a function 'whereAmI' which renders a country
// only based on GPS coordinates. For that, you will use a second API to geocode
// coordinates. So in this challenge, you’ll use an API on your own for the first time �
// Your tasks:
// PART 1
// 1. Create a function 'whereAmI' which takes as inputs a latitude value ('lat')
// and a longitude value ('lng') (these are GPS coordinates, examples are in test
// data below).
// 2. Do “reverse geocoding” of the provided coordinates. Reverse geocoding means
// to convert coordinates to a meaningful location, like a city and country name.
// Use this API to do reverse geocoding: https://geocode.xyz/api. The AJAX call
// will be done to a URL with this format:
// https://geocode.xyz/52.508,13.381?geoit=json. Use the fetch API and
// promises to get the data. Do not use the 'getJSON' function we created, that
// is cheating �
// 3. Once you have the data, take a look at it in the console to see all the attributes
// that you received about the provided location. Then, using this data, log a
// message like this to the console: “You are in Berlin, Germany”
// 4. Chain a .catch method to the end of the promise chain and log errors to the
// console
// 5. This API allows you to make only 3 requests per second. If you reload fast, you
// will get this error with code 403. This is an error with the request. Remember,
// fetch() does not reject the promise in this case. So create an error to reject
// the promise yourself, with a meaningful error message
// PART 2
// 6. Now it's time to use the received data to render a country. So take the relevant
// attribute from the geocoding API result, and plug it into the countries API that
// we have been using.
// 7. Render the country and catch any errors, just like we have done in the last
// lecture (you can even copy this code, no need to type the same code)
// The Complete JavaScript Course 31
// Test data:
// § Coordinates 1: 52.508, 13.381 (Latitude, Longitude)
// § Coordinates 2: 19.037, 72.873
// § Coordinates 3: -33.933, 18.474

//*************use async ajax html*************
// const loadCountryHtml = function (data, className = '') {
//   const [lan] = Object.values(data.languages)
//   const [currency] = Object.values(data.currencies)
//   let html = `
//   <article class="country ${className}">
//   <img class="country__img" src="${data.flags.png}" />
//   <div class="country__data">
//   <h3 class="country__name">${data.name.common}</h3>
//   <h4 class="country__region">${data.region}</h4>
//   <p class="country__row"><span>👫</span>${lan}</p>
//   <p class="country__row"><span>🗣️</span>${(data.population / 10000).toFixed(
//     1
//   )}</p>
//   <p class="country__row"><span>💰</span>${currency.name}</p>
//   </div>
//   </article>
//   `
//   document.querySelector('.countries').insertAdjacentHTML('beforeend', html)
//   document.querySelector('.countries').style.opacity = 1
// }
// const whereAmI = function (lat, lng) {
//   const url = `https://geocode.xyz/${lat},${lng}?geoit=json`
//   const countryUrl = `https://restcountries.com/v3.1/name/`
//   fetch(url)
//     .then((response) => {
//       if (!response.ok) {
//         throw new Error(
//           `something went wrong ${response.status}  ${response.statusText}`
//         )
//       }
//       return response.json()
//     })
//     .catch((err) => alert(err))
//     .then((data) => {
//       if (!data) return
//       console.log(`You are in ${data.city} , ${data.country}`)
//       return fetch(countryUrl + data.country)
//     })
//     .then((countryResp) => {
//       if (!countryResp) return
//       return countryResp.json()
//     })
//     .catch((error) => console.warn('countryRespCatch ', error))
//     .then((data) => {
//       if (!data) return
//       const [country] = data
//       console.log(country)
//       loadCountryHtml(country)
//     })
// }
// whereAmI(52.508, 13.381)
// // whereAmI(19.037, 72.873)
// // whereAmI(-33.933, 18.474)

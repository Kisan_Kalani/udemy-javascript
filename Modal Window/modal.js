'use strict'

const modal = document.querySelector('.modal')
const overlay = document.querySelector('.overlay')
const btnClose = document.querySelector('.close-modal')

const openModal = function () {
  modal.classList.remove('hidden')
  overlay.classList.remove('hidden')
}
const closeModal = function () {
  modal.classList.add('hidden')
  overlay.classList.add('hidden')
}

//exmple of queryselector with more  than 1  same class name
// const btnShowModals = document.querySelector('.show-modal') //will sleect only first show-modal
// console.log(btnShowModals)

//querySelector ALL
const btnShowModals = document.querySelectorAll('.show-modal') //returns NodeList of all Show-modals
// console.log(btnShowModals)
for (let i = 0; i < btnShowModals.length; i++) {
  //   console.log(btnShowModals[i].textContent)
  btnShowModals[i].addEventListener('click', openModal)
}
btnClose.addEventListener('click', closeModal)
overlay.addEventListener('click', closeModal)

document.addEventListener('keydown', function (event) {
  //   console.log(event.key)
  if (event.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal()
  }
})

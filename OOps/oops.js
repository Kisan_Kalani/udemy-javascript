'use strict'

//#region Constructor Function
// const Person = function (firstName, birthYear) {
//   //   console.log(this) //empty {}

//   //instances properties
//   this.firstName = firstName
//   this.birthYear = birthYear

//   //Never do this, use prototype method
//   //   this.calcAge = function(){
//   //       console.log(2037-this.birthYear)
//   //}
// }

// const kisan = new Person('kisan', 1995)
// console.log(kisan) //Person Object
// console.log(kisan instanceof Person)
//behind the scences of using NEW keyword to a function
// 1. New {} empty object is created
//2.function is called, this={}
//3.  {} linked to prototype
//4.function automaticallu returns {}, but not empty now

//#endregion

//#region Prototypes for methods
// console.log(Person.prototype)
// Person.prototype.calcAge = function () {
//   console.log(2037 - this.birthYear)
// }
// kisan.calcAge()

// console.log(Person.prototype.isPrototypeOf(kisan))
//#endregion

//#region ES6 Classes and Objects
//1. Class are not Hoisted , cannot use before declared in the code
//2. Classes are firt class citizen ,can pass them into func and reurn them from func
//3 . Classes are always executed in strict Mode
// class PersonCl {
//   constructor(firstName, birth) {
//     this.firstName = firstName;
//     this.birth = birth;
//   }

//   //Methods will be added to prototype of class
//   calcAge() {
//     console.log(2022 - this.birth)
//   }
// }

// const kisan = new PersonCl('kisan', 1995)
// console.log(kisan)
// kisan.calcAge()

// console.log(kisan.__proto__ === PersonCl.prototype)
// PersonCl.prototype.greet = function () {
//   console.log(`Hey ${this.firstName}`)
// }
// kisan.greet()
//#endregion

//#region Getters and Setters

// const account = {
//   owner: 'Kisan',
//   movements: [200, 30, 50, 100],
//   get latest() {
//     return this.movements.slice(-2).pop()
//   },

//   set latest(mov) {
//     this.movements.push(mov)
//   },
// }
// //getter use liked property
// console.log(account.latest)

// //setter used liked property
// account.latest = 50
// console.log(account)
//#endregion

//#region Static Method
// class Pers {
//   constructor(name, age) {}
// }
// Pers.hey = function () {
//   console.log('Hey There')
// }
// Pers.hey()
//#endregion

//#region Inheritance between  'Classes' :Constructor Functions
// const Person = function (firstName, birthYear) {
//   this.firstName = firstName
//   this.birthYear = birthYear
// }
// Person.prototype.calcAge = function () {
//   return 2037 - this.birthYear
// }
// const Student = function (firstName, birthYear, course) {
//   this.firstName = firstName
//   this.birthYear = birthYear
//   // use instead
//   Person(firstName, birthYear) // wrong,gives error as this is not defined
//   // use Call ,cannot use bind as it is for Functions
//   Person.call(this, firstName, birthYear)
//   this.course = course
// }
// // To Link Person to Student as Inheritance ,Need to Bind Prototypes using Object.Create ,Before Stdent Other Mehtods
// Student.prototype = Object.create(Person.prototype) //without this line ,this.calcAge() in introduce will give Error

// Student.prototype.introduce = function () {
//   console.log(
//     `Hey I am ${this.firstName} ,${this.calcAge()} aged,   I Study Course ${
//       this.course
//     }`
//   )
// }
// const mike = new Student('Mike', 1995, 'Coding')
// console.log(mike)
// mike.introduce()
//#region

//#region Inheritance between  'Classes' :ES 6 Classes Functions
// class PersonCl {
//   constructor(firstName, birthYear) {
//     this.firstName = firstName
//     this.birthYear = birthYear
//   }

//   calcAge() {
//     console.log(`i am ${2037 - this.birthYear}`)
//   }
// }
// class StudentCl extends PersonCl {
//   constructor(firstName, birthYear, course) {
//     super(firstName, birthYear)
//     this.course = course
//   }
// }
// const kisan = new StudentCl('Kisan', 1995, 'Material Science')
// kisan.calcAge()
//#region

//#region ******************* Encapsulation  -Classes Examples  Important *******************
// class Account {
//   constructor(owner, currency, pin) {
//     this.owner = owner
//     this.currency = currency
//     this.pin = pin
//     this.movements = []
//     this.locale = navigator.language
//   }

//   //public interface
//   deposit(mov) {
//     this.movements.push(mov)
//   }
//   withdrawal(mov) {
//     this.deposit(-mov)
//   }

//   approveloan(val) {
//     return true
//   }
//   requestLoan(val) {
//     if (this.approveloan(val)) {
//       this.deposit(val)
//       console.log('Loan Approved')
//     }
//   }
// }

// const acc1 = new Account('Kisan', 'INR', 7423)
// //for deposit and withdrwal
// //do not interact with  moevemts prop directly use methods
// // acc1.movements.push(500)
// // acc1.movements.push(-145)
// acc1.deposit(500)
// acc1.withdrawal(200)

// acc1.requestLoan(200)
// acc1.approveloan(200) //this must not be allowed
// console.log(acc1)
//#region

'use strict'

let Fundamentals_2_Loops
//Fundamental 2  Loops
// for (let rep = 1; rep <= 10; rep++) {
//   //   console.log('arr :', rep)
// }

// const arr = ['kinsa', 'kalani', 27, true]
// //forward  loop
// for (let index = 0; index < arr.length; index++) {
//   const element = arr[index]
//   //   console.log(element)
// }

// //reverse loop
// for (let index = arr.length - 1; index >= 0; index--) {
//   const element = arr[index]
//   //   console.log(element)
// }

//while loop
// let rep = 1
// while (rep <= 10) {
//   //   console.log('While Loop :' + rep)
//   rep++
// }

// //random dice looping out on number 6
// let dice = Math.trunc(Math.random() * 6 + 1)
// // console.log(dice)
// while (dice !== 6) {
//   console.log('Dice number:' + dice)
//   dice = Math.trunc(Math.random() * 6 + 1)
//   if (dice === 6) console.log('dice is 6 so looped out')
// }

//END - Fundamental 2  Loops

let Fundamentals_2_Objects
//Fundamental 2  Objects
// const kisan = {
//   name: 'Kisan',
//   birthYear: 1995,
//   job: 'Developer',
//   isLearning: true,
//   skillsWantToLearn: ['Coding', 'Gym', 'Guitar', 'Trekking', 'Acting'],
//   friends: ['Anjali', 'Niraj', 'Amruta'],
//   //   calcAge: function () {
//   //     // console.log(this)
//   //     return 2022 - this.birthYear
//   //   },
//   calcAge: function () {
//     this.age = 2022 - this.birthYear //create new prop in object
//     return this.age
//   },
// }
// console.log(kisan.calcAge())
// console.log(kisan.age)
// console.log(kisan.age)

// const challenge = `${kisan.name} has ${kisan.friends.length} friends but his best friend is ${kisan.friends[0]}`
// console.log(challenge)

//END Fundamental 2  Objects

let Fundamentals_2_Functions
//Fundamentals 2  -Functions
// function logger() {
//   console.log('calling func logger')
// }
// logger()

// function logMessage(message) {
//   console.log('This is the message ' + message)
// }
// logMessage('Kisan is learning JS')

// //returning from a function
// function returnFunc(apples, oranges) {
//   const juice = `Juice with ${apples} apples and ${oranges} oranges`
//   return juice
// }
// console.warn(returnFunc(5, 8))

// //anonymous function Expression
// const calcAge1 = function (birthYear) {
//   return 2037 - birthYear
// }
// console.log(calcAge1(1991))

// //Lambda (Arrow) Func Expression
// const calcAge2 = (birthYear) => {
//   return 2037 - birthYear
// }
// console.log(calcAge2(1991))

// //calling func inside another func
// function callInside(fruit) {
//   return fruit * 4
// }
// function calcFruits(apples, oranges) {
//   const applePieces = callInside(apples)
//   const orangePieces = callInside(oranges)
//   const juice = `Juice with ${applePieces} apples and ${orangePieces} oranges`
//   return juice
// }
// console.log(calcFruits(2, 3))

//END - Fundamentals 2

let Fundamentals_3_Datastructure_Destructuring
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30'

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  orders: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]]
  },
  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  OrderDelivery: function ({ mainIndex, time, starterIndex, address }) {
    //instead of obj object we can pass same name variables aand it will destructure accordingly and sequence doesn't matter
    // console.log(obj)
    // console.log(mainIndex, time, starterIndex, address)

    console.log(`Your Order  ,Manin Menu  ${this.mainMenu[mainIndex]}
  and starter Menu ${this.starterMenu[starterIndex]} will be delivered at ${time}
  on address ${address}`)
  },

  //example for spread Operator
  OrderPasta: function (ing1, ing2, ing3) {
    console.log(`Pasta is ready with ingredients ${ing1} , ${ing2} and ${ing3}`)
  },

  //example of rest pattern and parameters
  OrderPizza: function (maninIngredient, ...otherIngredient) {
    console.log(maninIngredient)
    console.log(otherIngredient)
  },
}

//live example of destructuring with OrderDelivery
// restaurant.OrderDelivery({
//   time: '22:30',
//   address: 'Waliv ,Vasai-East-401208',
//   mainIndex: 2,
//   starterIndex: 1,
// })

//destructuring an array into variables
// const arr = [1, 2, 5]
// const [a, b, c] = arr
// console.log(a, b, c)
// console.log(arr)

// const [first, second] = restaurant.categories //take first two
// console.log(first, second)
// const [firsts, , seconds] = restaurant.categories //take first and third,adding ',' for second
// console.log(firsts, seconds)
// let [first, second] = restaurant.categories //swapping first two
// console.log(first, second)

//using temp
// const temp = first
// first = second
// second = temp
// console.log(first, second)

//Swap using destruting
// [second,first] = [first,second]
// console.log(first,second)

//destruting arras from fundtion
// const [starterCourse, mainCourse] = restaurant.orders(2, 0)
// console.log(starterCourse, mainCourse)

//Nested Destucturing
// const nested = [2, 4, [6, 7]]

// const [i, , j] = nested //output i=(2) and j=[6,7]
// console.log(i, j)

// const [i, , [j, k]] = nested //output i=2,j=6,k=7
// console.log(i, j, k)

//Default Values
// const [p = 1, q = 1, r = 1] = [8,9]
// console.log(p, q, r)

//Destructuring Objects
//rule:should have same name and use {} braces ,also can give another name using :
// const { name: resName, openingHours: hours, categories: cat } = restaurant
// console.log(resName, hours, cat)

//Mutating variables
// let k = 111
// let l = 999
// const obj = { k: 7, l: 16, m: 6 }
// console.log(({ k, l } = obj)) //output :k=7 and l=16

//nested destructuring
// openingHours: {
//   fri: {
//     open: 11,
//     close: 23,
//   }, sat: {
//   open: 0, // Open 24 hours
//   close: 24,
//   }
// }
// const { fri } = restaurant.openingHours
// console.log(fri) //{close: 23 , open: 11}

// const {
//   sat: { open: o, close: cl },
// } = restaurant.openingHours
// console.log(o, cl) //with naming and nesting

let Fundamentals_3_Datastructure_SpreadOperator
// const arrs = [6, 7, 8]
// const badNewArr = [1, 2, arr[0], arr[1], arr[2]] //without SpreadOperator
// console.log(badNewArr) //output[1,2,6,7,8]

// const newarr = [1, 2, ...arrs] // with SpreadOperator (...)
// console.log(newarr) //output[1,2,6,7,8]

//log indiviual element of arr without comma
// console.log(...newarr) // 1 2 6 7 8

//add item in menu example
// const newmenu = [...restaurant.mainMenu, 'Goucci']
// console.log(newmenu)

//copy array
// const copynewArr = [...restaurant.mainMenu]
// console.log(copynewArr)

//join two arrays  arr and newmenu
// const joinedArr = [...arr, ...newmenu]
// console.log(joinedArr)

//Using in Literals like string,maps,sets BUT not Objects
// const str = 'kisan'
// const letters = [...str, ' ', '.R.']
// console.log(letters) //output:['k','i','s','a','n',' ','.R.']

//example of spread with restauratnt OrderPasta function
// const ingredients = [
//   prompt("Let's make Pasta,Ingredient 1 "),
//   prompt('Ingredient 2 '),
//   prompt('Ingredient 3 '),
// ]
// console.log(ingredients)
// restaurant.OrderPasta(...ingredients)

let Fundamentals_3_Datastructure_RestPAttern_Parameters
//opposite of Spread(...) operator
// It must be only one and last element

// 1) For Destructuring
//example of spread
// const arr = [1, 2, ...[8, 9, 6]]
// console.log(arr)

//rest pattern & destructring
// const [a, b, ...others] = [1, 2, 3, 6, 9]
// console.log(a, b, others)

//rest pattern & destructring with Spread
// const [pizza, , Risotto, ...otherfoods] = [
//   ...restaurant.mainMenu,
//   ...restaurant.starterMenu,
// ]
// console.log(pizza, Risotto, otherfoods) //output -> pizza,Risotto, arra[restremaining foods]

//take sat as single and fri thur as weekdays as object
// const { sat, ...weekdays } = restaurant.openingHours
// console.log(sat) //sat object
// console.log(weekdays) //thu and fri object

// 2) For Functions
//adding numbers unlimited  params
// const addNumbers = function (...numbers) {
//   let sum = 0
//   for (let i = 0; i < numbers.length; i++) {
//     sum = sum + numbers[i]
//   }
//   console.log('sum :', sum)
// }
// addNumbers(2, 6)
// addNumbers(5, 9, 6, 5, 8)
// addNumbers(1, 5, 9, 6, 8, 4, 2, 3)

//using array for addNumber function adn passing like spread
// const x = [4, 5, 6]
// addNumbers(...x)

//creating orderPizza mehto in restuarant ehihc tak one ing and rest optional
// restaurant.OrderPizza('Mushroom', 'Olive', 'Jackfruit') //output-> mushroom, Array[olive ,jackfruit]
// restaurant.OrderPizza('OnlyOne INgredient') //other will be emoty array

let Fundamentals_3_Datastructure_LoopingArrays_For_OF_Loop
// const menu = [...restaurant.starterMenu, ...restaurant.mainMenu]
// console.log(menu)
//1)  For Of Loop
// for (let item of menu) {
//   console.log(item)
// }

//2) For of Loop With Index using entries()
// for (let item of menu.entries()) {
//   console.log(`${item[0] + 1} : ${item[1]}`)
//   console.log()
// }
// console.log([...menu.entries()])

//3) For of Loop With Index using entries() and destructuring
// for (let [i, el] of menu.entries()) {
//   console.log(`${i + 1} : ${el}`)
// }

let Fundamentals_3_Datastructure_Enhance_Obj_Literals
//Enhance Object Literals
//1) can put object out and use same name eg: openingHours
//2)can remove keyword Function from Function & pass Params Directly
//3) can Manipulate obj key name eg:openingHours and Weekday

// const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri']
// const openingHours = {
//   [weekdays[2]]: {
//     //Enhance Obj Literal (3)
//     open: 12,
//     close: 22,
//   },
//   [weekdays[4]]: {
//     //Enhance Obj Literal (3)
//     open: 11,
//     close: 23,
//   },
//   [`days-${2 + 5}`]: {
//     //Enhance Obj Literal (3)
//     open: 0,
//     close: 24,
//   },
// }
// const enhanceObjLiterals = {
//   name: 'Classico Italiano',
//   openingHours, //Enhance Obj Literal (1)
//   orders(
//     starterIndex,
//     mainIndex //Enhance Obj Literal(2)
//   ) {
//     return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]]
//   },
// }

let Fundamentals_3_Datastructure_Optional_Chaninig //(?.)
// console.log(restaurant.openingHours.mon.open) //gives error

// using optional chaining(?.)
// console.log(restaurant.openingHours.mon?.open) //undefined

// Example UseCase (?.)
// const days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
// for (const day of days) {
//   console.log(day)
//   const op = restaurant.openingHours[day]?.open ?? 'closed'
//   console.log(`At ${day}, We are Open at ${op}`)
// }

//Optional Chaaning on Methods
// console.log(restaurant.orders(0, 1)) //method exists
// console.log(restaurant.ordersPiasa?.(0, 1) ?? 'Method does not exisit') //method does not exists

//Optional chaning with arrays and objects
// const users = [{ name: 'kisan', email: 'kalanik002@gmail.com' }]
// console.log(users[1].name) //error
// console.log(users[1]?.name ?? 'user empty') //undefined userempty
// console.log(users[0]?.name) //kisan

let Fundamentals_3_Datastructure_LoopingObjecs_keysvalues_entries
//1) Property Names -->Object.Keys
// const propNames = Object.keys(restaurant.openingHours)
// console.log(propNames)
// let openStr = `We are open ${propNames.length} days :`
// for (const day of propNames) {
//   openStr += `${day},`
// }
// console.log(openStr)

//2) Property Values-->Object.Values
// const propValues = Object.values(restaurant.openingHours)
// console.log(propValues)

// 3) Object Entries
// menu.entries() = for arrays use .entries()
// For Objects 👇👇
// const objEntries = Object.entries(restaurant.openingHours)
// console.log(objEntries)
// using for of loop with destruiting to write STRing
// for (const [key, { open, close }] of objEntries) {
//   console.log(`On ${key} , We are Open at ${open} and Close at ${close}`)
// }

let Fundamentals_3_Datastructure_Sets
// const orderSets = new Set(['pizza', 'pasta', 'pizza', 'risotto', 'pasta'])
// console.log(orderSets)
// console.log(new Set('kisan'))
// console.log(orderSets.size) //3
// console.log(orderSets.has('pizza')) //true/false
// console.log(orderSets.add('creamy'))
// console.log(orderSets.delete('pizza'))
// // orderSets.clear()
// //set and loop
// for (const order of orderSets) {
//   console.log(order)
// }

//Spread with Sets gives unique array
// const staff = ['waiter', 'manager', 'boy', 'waiter', 'boy', 'waiter']

// const staffunique = [...new Set(staff)] //add [] and ...
// console.log(staffunique)

let Fundamentals_3_Datastructure_Maps
// const rest = new Map()
// rest.set('name', 'kisan kalani')
// rest.set(1, 'Vasai East,Waliv')
// rest.set(2, 'Malad West, Malwani')
// rest
//   .set(true, 'We are Open😀')
//   .set(false, 'we are Closed😢')
//   .set('categories', ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'])

// //using queryselector to map element
// rest.set(document.querySelector('p'), 'Heading')
// console.log(rest)

// console.log(rest.size)
// console.log(rest.get('name'))
// console.log(rest.has('name'))
// rest.delete(2)

// const arra = [1, 2]
// rest.set(arra, 'Testing arra as key')
// console.log(rest.get(arra))

// console.log(rest)

let Fundamentals_3_Datastructure_Maps_Iteration
// const quiz = new Map([
//   ['question', 'What is the best Programming Lang?'],
//   [1, 'C#'],
//   [2, 'Java'],
//   [3, 'javscript'],
//   ['correct', 3],
//   [true, 'Yes..!!You are Correct🥳🎊'],
//   [false, 'Try Again'],
// ])
// console.log(quiz)

//changign Object to MAP
// const hours = new Map(Object.entries(restaurant.openingHours))
// console.log(hours)

//Quiz App
// console.log(quiz.get('question'))
// for (const [key, value] of quiz) {
//   if (typeof key === 'number') {
//     console.log(`Option ${key} : ${value}`)
//   }
// }
// const answer = 3 //Number(prompt('Your Answer!'))
// console.log(answer)

// console.log(answer === quiz.get('correct') ? quiz.get(true) : quiz.get(false))
// console.log(quiz.get(quiz.get('correct') === 3))
// console.log(...quiz)

let Fundamentals_3_Datastructure_Workign_with_Strings_1
const airline = 'TAP Air Portugal'
const plane = 'A320'
//basic
// console.log(plane[0]) //A
// console.log(plane[1]) //3
// console.log('B737'[1]) //7
// console.log(airline.length) //16

//Strings mehtods //always return new string so store it to use
// console.log(airline.indexOf('r')) //6
// console.log(airline.lastIndexOf('r')) //10
// console.log(airline.indexOf('Portugal')) //8 //case sensitive
// console.log(airline.indexOf('portugal')) //-1 //case sensitive
// console.log(airline.slice(4)) //Air Portugal
// console.log(airline.slice(4, 7)) //Air
// console.log(airline.slice(0, airline.indexOf(' '))) //TAP
// console.log(airline.slice(airline.lastIndexOf(' ') + 1)) //Portugal
// console.log(airline.slice(-5)) //tugal

//example 1)
// const checkMiddleSeat = function (seatno) {
//   //if string contains last char as B or C then it is a Middle seat
//   const lastChar = seatno.slice(-1)
//   console.log(
//     `seatno ${seatno} : ${
//       lastChar === 'B' || lastChar === 'C' ? 'Middle' : 'Other'
//     }`
//   )
// }
// checkMiddleSeat('11A')
// checkMiddleSeat('124C')

let Fundamentals_3_Datastructure_Workign_with_Strings_2
//Upper and Lower Case
// console.log(airline.toUpperCase())
// console.log(airline.toLowerCase())

//example 2
// const jonas = 'jONaS' //try output 'Jonas'
// console.log(jonas[0].toUpperCase() + jonas.slice(1).toLowerCase())
//example 3 //check email
// const email = 'kisan@gmail.com'
// const enEmail = ' KiSan@Gmail.CoM \n'
// const changed = enEmail.trim().toLowerCase()
// console.log(changed === email ? 'Email Matched' : 'Email Incorrect')

//Replacing
// const priceGB = '277,68\u20AC'
// const priceUS = priceGB.replace('\u20AC', '$').replace(',', '.')
// console.log(priceUS)

// const announcement = 'All passengercome to boarding door 23.Boarding door 23'
// console.log(announcement.replaceAll('door', 'gate'))

//Includes and stratswith ,endsWith
// const pale = 'A356neo'
// console.log(pale.includes('neo')) //true
// console.log(pale.startsWith('A5')) //false
// console.log(pale.endsWith('neo')) //true

let Fundamentals_3_Datastructure_Workign_with_Strings_3
//Split  --> returns array of strings
// const nice = 'a+Very+Nice+string'
// console.log(...nice.toLowerCase().split('+'))

// const naeem = 'kisan kalani'
// const [firstName, lastName] = naeem.split(' ')
// console.log(firstName, lastName)

//Join
// const arrName = ['Mr.', 'kISaN', 'kalani']
// const [i, f, l] = arrName
// const newArr = [
//   i,
//   f[0].toUpperCase() + f.slice(1).toLowerCase(),
//   l.toUpperCase(),
// ]
// console.log(newArr.join(' '))

//example :Camel Case name
// const CamelCaseName = function (names) {
//   const arr = names.split(' ')
//   const namearr = []
//   for (const str of arr) {
//     // const newName = str[0].toUpperCase() + str.slice(1).toLowerCase()

//     const newName = str.replace(str[0], str[0].toUpperCase())
//     namearr.push(newName)
//   }
//   console.log(namearr.join(' '))
// }
// CamelCaseName('julian ann smith jenny')
// CamelCaseName('adrain kisan rickson')

//Padding ->adding no of chars until desired length
// const message = 'Go To Gate no 23'
// console.log(message.padStart(25, '*')) //*********Go To Gate no 23

//Exmaple
// const MaskCreditCard = function (number) {
//   const str = number + ''
//   const last = str.slice(-4)
//   return last.padStart(str.length, '*')
// }
// console.log(MaskCreditCard('256558962'))
// console.log(MaskCreditCard(652985156325))

let section_10_FUNCTIONS
//1) Default Parmateres
// const bookings = []
// const createBooking = function (flighNo, noOfPass = 2, price = 199 * noOfPass) {
//   const booking = {
//     flightno: flighNo,
//     noOfPass: noOfPass,
//     price: price,
//   }
//   console.log(booking)
//   bookings.push(booking)
// }
// createBooking('LH25D')
// createBooking('LH25D', 5, 241)

//2) Value VS Reference .how argments works
// const flight = 'LH235'
// const jonas = {
//   name: 'Jonas',
//   passport: 256234126565,
// }
// const checkIn = function (flightNo, pass) {
//   flightNo = 'KD652'
//   pass.name = 'MR. Jonas Schemidth'

//   if (pass.passport === 256234126565) {
//     console.log('Checked In')
//   } else {
//     console.log('Wrong Passport')
//   }
// }
// checkIn(flight, jonas)
// console.log(flight) //primitive type so no change
// console.log(jonas) //reference type soJonas name also chnaged

//3)First class or Higher Order functions
//First class func are just values , it is a concept
//Hight Ordr func has three types

//A) Accepting Callback functions
// const oneword = function (str) {
//   return str.replaceAll(' ', '').toLowerCase()
// }
// const onUpperFun = function (str) {
//   const [first, ...others] = str.split(' ')
//   return `${first.toUpperCase()} ${others.join(' ')}`
// }
// // console.log(oneword('this is just the string'))
// // console.log(onUpperFun('this is just the string'))
// const higherOrderFunc = function (str, fnc) {
//   console.log('Original String: ', str)
//   console.log('Changed String: ', fnc(str))
//   console.log('Called Function: ', fnc.name)
// }
// higherOrderFunc('this is Javasacript HigherORder', onUpperFun)

// B) Function Returns Other Functions
// const greet = function (greet) {
//   return function (name) {
//     console.log(`${greet} ${name}`)
//   }
// }
// const greeter = greet('Hey')
// greeter('Jonas')
// greeter('Jackie')
// greet('Hello')('Kisan')

//using arrow Func
// const greet = (greet) => (name) => {
//   console.log(`${greet} ${name}`)
// }
// const greeter = greet('Hey')
// greeter('Jonas')
// greeter('Jackie')
// greet('Hello')('Kisan')

// C) Call and Apply (this keyword)
// const lufthana = {
//   airline: 'Lufthasa',
//   iataCode: 'LH',
//   bookings: [],
//   book(flightNo, name) {
//     console.log(
//       `${name} booked a seat on ${this.airline} Flight ${this.iataCode}${flightNo}`
//     )
//     this.bookings.push({
//       flight: `${this.iataCode}${flightNo}`,
//       name,
//     })
//   },
// }
// lufthana.book(154, 'Jonas schmide')
// lufthana.book(195, 'Jackie Bagnani')
// console.log(lufthana)
// const euroWings = {
//   airline: 'Euro Wings',
//   iataCode: 'EW',
//   bookings: [],
// }
// const swiss = {
//   airline: 'Swiss Internatials',
//   iataCode: 'SW',
//   bookings: [],
// }

// create book for Eurowings
// const book = lufthana.book
// book(23, 'Johnny Depp')//gives error becuse it isnormal func and tis does not work on normal functions

// using call()  || apply()
// book.call(euroWings, 23, 'Jonny Depp')
// book.apply(euroWings, [23, 'Jonny Depp']) //need arg as array
// console.log(euroWings)
// console.log(lufthana)

// Bind() Mehtod
// const bookEW = book.bind(euroWings)
// const bookSW = book.bind(swiss)
// bookSW(45, 'Serah Willliams')
// bookEW(4546, 'Rehta JOsna')
// Bind with Event Listeners
// lufthana.plane = 300
// lufthana.buyPlane = function (plane) {
//   console.log(this)
//   this.plane++
//   console.log(this.plane)
// }
// const btnBuyPlane = document.querySelector('.buy')
// btnBuyPlane.addEventListener('click', lufthana.buyPlane.bind(lufthana))

// Partial Application
// const addTax = (rate, value) => {
//   return value + value * rate
// }
// // console.log(addTax(0.1, 200))
// // const addVatTax = addTax.bind(null, 0.23)
// // console.log(addVatTax(100))
// const addVatTax = function (value) {
//   return addTax(0.23, value)
// }
// console.log(addVatTax(200))

let section_11_Working_with_ARRAYS
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
  movementsDates: [
    '2019-11-18T21:31:17.178Z',
    '2019-12-23T07:42:02.383Z',
    '2020-01-28T09:15:04.904Z',
    '2020-04-01T10:17:24.185Z',
    '2020-05-08T14:11:59.604Z',
    '2020-05-27T17:01:17.194Z',
    '2020-07-11T23:36:17.929Z',
    '2020-07-12T10:51:36.790Z',
  ],
  currency: 'EUR',
  locale: 'pt-PT', // de-DE
}

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
}

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
}

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
}
// const accounts = [account1, account2, account3, account4]
const accounts = [account1, account2]
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300]
//1)Simple Array Methods
// const testArr = ['a', 'b', 'c', 'd', 'e', 'f']
// const TestArr2 = ['k', 'j', 'i', 'h', 'g']
//A) slice() //doesnot change orig Arry
// console.log(testArr.slice(1, 3)) ///b,c
// console.log(testArr.slice(2)) //c,d,e,f
// console.log(testArr.slice(-3)) //d,e,f
// console.log(testArr.slice(1, -2)) ///b,c,d
// //To Create copy of Original Array
// console.log(testArr.slice()) //Copy of original arr
// console.log([...testArr]) //copy using spread
// console.log(testArr.slice())

//B) splice() //changes original Arr also
// console.log(testArr.splice(2)) //removes a ,b
// console.log(testArr) //changed now

//C) Reverse //changes original array

// console.log(arr2.reverse())
// console.log(arr2)

//C) Concal //doesnot change orig Arry
// console.log(testArr.concat(TestArr2))
// console.log([...testArr, ...TestArr2]) //const using spread

// D) JOIN() //doesnot change orig Arry
// console.log(testArr.join('-'))

// 2)The New At Method  //at()
// const TestArr3 = [23, 11, 65]
// console.log(TestArr3[0]) //old way
// console.log(TestArr3.at(0)) //using at()

// Old way of getting last element
// console.log(TestArr3[TestArr3.length - 1]) //65
// console.log(TestArr3.slice(-1)[0]) //65
// New way of getting last element using at()
// console.log(TestArr3.at(-1)) //65

// 3)Looping Arrays :forEach

//For loop
// for (const  mov of movements) {
// for (const [i, mov] of movements.entries()) {
//   const str = mov > 0 ? 'You Deposited ' : 'You Withdraw'
//   console.log(`${i + 1}) ${str} ${Math.abs(mov)}`)
// }

//ForEach  loop //Cannot use  break; and continue;
// movements.forEach(function (mov, i, origArray) {
//   const str = mov > 0 ? 'You Deposited ' : 'You Withdraw'
//   console.log(`${i + 1})  ${str} ${Math.abs(mov)}`)
// })

//ForEach with Maps and Sets
//With Map
// const currencies = new Map([
//   ['USD', 'United States dollar'],
//   ['EUR', 'Euro'],
//   ['GBP', 'Pound sterling'],
// ])
// console.log(currencies)
// currencies.forEach(function (value, key, mapArr) {
//   console.log(`${key} : ${value}`)
// })

//With Set
// const uniqueCurr = new Set(['USD', 'GBP', 'INR', 'GBP', 'INR', 'EURO', 'USD'])
// // console.log(uniqueCurr)
// uniqueCurr.forEach(function (value, _, setArr) {
//   console.log(`${value}`)
// })

//4) Data Transformation :MAP ,FILTER,REDUCE,FIND,FindIndex, SOME,EVERY, FLAt and FLATMAP
//A) MAP Method
// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300]
// const euroToUsd = 1.1 //convert euro to dollar   ( * 1.1)
// const dollarMove = movements.map((value, index, origArr) => {
//   return Math.round(value * euroToUsd, 2)
// })
// console.log(dollarMove)

//B) FILTER Method
// const depositArr = movements.filter(function (val, index, origArr) {
//   return val < 0
// })
// console.log(depositArr)

//C) REDUCE Method
// const reduceValue = movements.reduce(function (accumulator, current, index) {
//   // console.log(`Iterator ${index} : ${accumulator} accumulator `)
//   return accumulator + current
// }, 0)
// console.log(reduceValue)

//Using reduce for maximum value
// const maxValue = movements.reduce(function (acc, mov) {
//   if (acc > mov) {
//     //for min change to <
//     return acc
//   } else {
//     return mov
//   }
// }, movements[0])
// console.log(movements)
// console.log(maxValue)

//D) FIND Method
// const account = accounts.find((acc) => acc.owner === 'Sarah Smith')
// console.log(account)
//using for Of
// let acc = {}
// for (const account of accounts) {
//   if (account.owner === 'Sarah Smith') {
//     acc = account
//   }
// }
// console.log(acc)

//E) SOME and EVERY
// console.log(movements)
// console.log(movements.includes(-130)) //for eqaulity
// console.log(movements.some((mov) => mov > 500)) //true//for comparison
// console.log(movements.every((mov) => mov > 3000)) //false

//F) Flat and FlatMap
// const arr = [[1, 2, 3], [4, 5, 6, 7, 8], 9, 10, 11]
// const [ar, ar2, ...other] = arr
// console.log([...ar, ...ar2, ...other])
// using flat
// console.log(arr.flat())

// const arrDeep = [[1, [2, 3]], [4, [5, [6, 7]], 8], 9, 10, 11]
// console.log(arr.flat(2))

//Find OverallBalnces for all accounts in bank Using Flat()
// const overAllBal = accounts
//   .map((mov) => mov.movements)
//   .flat()
//   .reduce((acc, cur) => acc + cur, 0)
// console.log(overAllBal)
// // Using FlatMAp()
// const overAllBalFlatMap = accounts
//   .flatMap((mov) => mov.movements)
//   .reduce((acc, cur) => acc + cur, 0)
// console.log(overAllBalFlatMap)

//5) Sorting Arrays  //changes original array
// const owners = ['jonas', 'jack', 'adam', 'martha']
// console.log(owners.sort())
// console.log(owners)

// console.log(movements)

//Return >0 then A before B
//Return <0 then B before A
// movements.sort((a, b) => {
//   //ASC
//   if (a > b) return 1
//   if (a < b) return -1
// })
// movements.sort((a, b) => {
//   //DESC
//   if (a < b) return 1
//   if (a > b) return -1
// })

// movements.sort((a, b) => a - b) //ASC
// movements.sort((a, b) => b - a) //DESC
// console.log(movements)

//6) Programmatically Creating and Filling Array
// const arr = [1, 5, 3, 6, 9, 8, 5]
// console.log(new Array([1, 6, 8, 6, 7, 1, 2, 2]))

//Empty Array + Fill method
// const x = new Array(7)
// console.log(x)
// console.log(x.map(()=>5)) gives empty array same does nothing
// console.log(x.fill(1)) // [1, 1, 1, 1, 1, 1, 1]
// console.log(x.fill(1, 3, 5)) //[empty × 3, 1, 1, empty × 2]

// arr.fill(23, 2, 6)
// console.log(arr) //[1, 5, 23, 23, 23, 23, 5]

//Array.from()
// const y = Array.from({ length: 7 }, () => 1)
// console.log(y) //[1, 1, 1, 1, 1, 1, 1]

// const z = Array.from({ length: 7 }, (_, i) => i + 1)
// console.log(z) // [1, 2, 3, 4, 5, 6, 7]

//EXAMPLE : Dice Roll numbers from 1 to 100
// const dice = Array.from({ length: 100 }, () =>
//   Math.floor(Math.random() * 6 + 1)
// )
// console.log(dice)
//Array Practice
//1) Calculate total Deposit in the bank
//Mehtod A
// let totdepoBal = 0
// accounts.forEach((account) => {
//   console.log(account.movements)
//   totdepoBal =
//     totdepoBal +
//     account.movements
//       .filter((mov) => mov > 0)
//       .reduce((acc, cur) => acc + cur, 0)
// })
// console.log(totdepoBal)

//Mehtod B
// const totdepoBal = accounts
//   .flatMap((acc) => acc.movements)
//   .filter((mov) => mov > 0)
//   .reduce((acc, cur) => acc + cur, 0)
// console.log(totdepoBal)

//2) how many (length) deposits greater than 1000 in bank
// const numDepoThousands = accounts
//   .flatMap((acc) => acc.movements)
//   // .filter((mov) => mov >= 1000).length
//   .reduce((count, cur) => (cur >= 1000 ? count + 1 : count), 0)
// console.log(numDepoThousands)

//3) ++ prefixed ,++ suffixed
// let a = 10
// console.log(a++) //output: 10
// console.log(a) //output: 11
// let b = 12
// console.log(++b) //output: 13
// console.log(b) //output: 13

// 4) calc deopisit and sum all in one using reduce
// // const calcSumAll = accounts
// const { deposit, withdrawal } = accounts
//   .flatMap((acc) => acc.movements)
//   .reduce(
//     (accSum, cur) => {
//       // cur >= 0 ? (accSum.deposit += cur) : (accSum.withdrawal += cur)

//       accSum[cur > 0 ? 'deposit' : 'withdrawal'] += cur
//       return accSum
//     },
//     { deposit: 0, withdrawal: 0 }
//   )
// // console.log(calcSumAll.deposit, calcSumAll.withdrawal)
// console.log(deposit, withdrawal)

// reduce output as Array
// const calcSumAll = accounts
// const [a, b] = accounts
//   .flatMap((acc) => acc.movements)
//   .reduce(
//     (accSum, cur) => {
//       // cur > 0 ? (accSum[0] += cur) : (accSum[1] += cur)
//       accSum[cur > 0 ? 0 : 1] += cur
//       return accSum
//     },
//     [0, 0]
//   )
// // console.log(calcSumAll)
// console.log(a, b)

// 5) Title casing this is a nice title -> This Is a Nice Title
// const resulStr = function (str) {
//   const exceptions = ['a', 'and', 'an', 'the', 'but', 'or', 'on', 'in', 'with']
//   const capitalize = (str) => str.replace(str[0], str[0].toUpperCase())
//   const titleCasing = str
//     .toLowerCase()
//     .split(' ')
//     .map((word) =>
//       // exceptions.includes(word)
//       //   ? word
//       //   : word.replace(word[0], word[0].toUpperCase())

//       exceptions.includes(word) ? word : capitalize(word)
//     )
//     .join(' ')
//   return capitalize(titleCasing)
// }

// console.log(resulStr('this is a nice title'))
// console.log(resulStr('this is a LONG title but not too long'))
// console.log(resulStr('and here is another title with an EXAMPLE'))

let section_12_Numbers
// use data from section_11_Working_with_ARRAYS

// console.log(23 === 23.0) //true by default numbers are decimals

// type Coversion
// console.log(Number('23'))
// console.log(+'23')

//parseInt
// console.log(Number.parseInt('30px')) //output : 30
// console.log(Number.parseInt('en30')) //output : NaN
// console.log(Number.parseInt('2.53rem')) //output : 2
//parseFloat
// console.log(Number.parseFloat('2.53rem')) //output : 2.53
// console.log(Number.parseFloat(' 2.87rem ')) //output : 2.87

// 1) MATH Function and Methods
// A) Square Root -->Math.sqrt
// console.log(Math.sqrt(25)) // 5
// console.log(25 ** (1 / 2)) // 5

// B) MAX and MIN
// console.log(Math.max(10, 11, 23, 7, 6)) //23
// console.log(Math.min(10, 11, 23, 7, 6)) //6
// console.log(Math.max(10, 11, '23', 7, 6)) //23
// console.log(Math.max(10, 11, '23px', 7, 6)) //NaN
// const randomInt = (min, max) => Math.floor(Math.random() * (max - min)) + min
// console.log(randomInt(1, 10))
// console.log((23.5655).toFixed(2))

// C) Remainder Operator
// console.log(5 / 2) //2.5
// console.log(5 % 2) //1 ->reminder after division
//even or odd numbers
// const numArr = [6, 5, 1, 2, 4, 10, 95, 27, 28, 19, 16, 35, 30, 52, 43]
// console.log(numArr.filter((n) => n % 2 === 0)) //even numbers
// console.log(numArr.filter((n) => n % 2 !== 0)) //odd numbers
// check banklist_arr.js for practice for even odd coloring

let section_12_Dates
//Create a Date
// console.log(new Date()) //Thu Oct 27 2022 09:32:45 GMT+0530 (India Standard Time)
// console.log(new Date('Thu Oct 27 2022')) //Thu Oct 27 2022 00:00:00 GMT+0530 (India Standard Time)
// console.log(new Date('December 24,2022')) //Sat Dec 24 2022 00:00:00 GMT+0530 (India Standard Time)
// console.log(new Date(account1.movementsDates[0])) //Tue Nov 19 2019 03:01:17 GMT+0530 (India Standard Time)
// console.log(new Date(2031, 10, 28, 11, 45, 30)) //Fri Nov 28 2031 11:45:30 GMT+0530 (India Standard Time)
// console.log(new Date(0)) //Thu Jan 01 1970 05:30:00 GMT+0530 (India Standard Time)
// console.log(new Date(3 * 24 * 60 * 60 * 1000)) //Sun Jan 04 1970 05:30:00 GMT+0530 (India Standard Time) //timestamp=259200000

//Date methods
// const future = new Date(2031, 10, 28, 11, 45) //Fri Nov 28 2031 11:45:00 GMT+0530 (India Standard Time)
// console.log(future.getFullYear())
// console.log(future.getMonth())
// console.log(future.getDate())
// console.log(future.getDay())
// console.log(future.getHours())
// console.log(future.getMinutes())
// console.log(future.getSeconds())
// console.log(future.toISOString()) //2031-11-28T06:15:00.000Z
// console.log(future.getTime()) //get Timestamp
// console.log(Date.now()) //current timeStamp

// future.setFullYear(2040)
// console.log(future)

let section_12_Dates_Operations_INTL
// const future = new Date(2031, 10, 28, 11, 45)
// console.log(+future)
// const calcdays = (date1, date2) => (date2 - date1) / (1000 * 60 * 60 * 24)

// const days1 = calcdays(new Date(2022, 10, 15), new Date(2022, 10, 31))
// console.log(days1)

//Internationalization API For Dates and Numbers

//Dates
// const now = new Date()
// const formatedIntlDate1 = new Intl.DateTimeFormat('en-US').format(now) ///Seach for  ISO language Codes tables
// console.log(formatedIntlDate1)

// const options = {
//   hour: 'numeric',
//   minute: 'numeric',
//   day: 'numeric',
//   month: 'long',
//   year: 'numeric',
//   weekday: 'long',
// }

// const locale = navigator.language
// // console.log(locale)
// const formatedIntlDate2 = new Intl.DateTimeFormat(locale, options).format(now)
// console.log(formatedIntlDate2)

//NUmbers
// const num = 3564489.489
// const options = {
//   style: 'currency', //unit ,percent,currency
//   unit: 'mile-per-hour',
//   currency: 'EUR',
//   useGrouping: false,
// }
// console.log('US : ', new Intl.NumberFormat('en-US', options).format(num))
// console.log('GB : ', new Intl.NumberFormat('de-DE', options).format(num))
// console.log('PT : ', new Intl.NumberFormat('pt-PT', options).format(num))
// console.log('Syria : ', new Intl.NumberFormat('ar-SY', options).format(num))
// console.log(
//   'Browser  : ',
//   new Intl.NumberFormat(navigator.language, options).format(num)
// )

let section_14_OOPS
//Refer to OOPs.js file

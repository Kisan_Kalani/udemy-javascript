'use strict'

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
  movementsDates: [
    '2019-11-18T21:31:17.178Z',
    '2019-12-23T07:42:02.383Z',
    '2020-01-28T09:15:04.904Z',
    '2020-04-01T10:17:24.185Z',
    '2020-05-08T14:11:59.604Z',
    '2022-10-28T17:01:17.194Z',
    '2022-10-30T23:36:17.929Z',
    '2022-11-01T10:51:36.790Z',
  ],
  currency: 'EUR',
  locale: 'pt-PT', // de-DE
}

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
}

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
}

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
}

// const accounts = [account1, account2, account3, account4]
const accounts = [account1, account2]
// Elements
const labelWelcome = document.querySelector('.welcome')
const labelDate = document.querySelector('.date')
const labelBalance = document.querySelector('.balance__value')
const labelSumIn = document.querySelector('.summary__value--in')
const labelSumOut = document.querySelector('.summary__value--out')
const labelSumInterest = document.querySelector('.summary__value--interest')
const labelTimer = document.querySelector('.timer')

const containerApp = document.querySelector('.app')
const containerMovements = document.querySelector('.movements')

const btnLogin = document.querySelector('.login__btn')
const btnTransfer = document.querySelector('.form__btn--transfer')
const btnLoan = document.querySelector('.form__btn--loan')
const btnClose = document.querySelector('.form__btn--close')
const btnSort = document.querySelector('.btn--sort')

const inputLoginUsername = document.querySelector('.login__input--user')
const inputLoginPin = document.querySelector('.login__input--pin')
const inputTransferTo = document.querySelector('.form__input--to')
const inputTransferAmount = document.querySelector('.form__input--amount')
const inputLoanAmount = document.querySelector('.form__input--loan-amount')
const inputCloseUsername = document.querySelector('.form__input--user')
const inputClosePin = document.querySelector('.form__input--pin')

const formatMovementDate = function (movDate, locale) {
  const calcdays = (date1, date2) =>
    Math.round(Math.abs((date2 - date1) / (1000 * 60 * 60 * 24)), 0)

  const dayspassed = calcdays(movDate, new Date())
  if (dayspassed === 0) return 'Today'
  if (dayspassed === 1) return 'Yesterday'
  if (dayspassed <= 7) return `${dayspassed} days ago`
  else {
    // const day = `${movDate.getDate()}`.padStart(2, 0)
    // const month = `${movDate.getMonth() + 1}`.padStart(2, 0)
    // const year = movDate.getFullYear()
    // const hr = `${movDate.getHours()}`.padStart(2, 0)
    // const min = `${movDate.getMinutes()}`.padStart(2, 0)
    // return `${day}/${month}/${year} ${hr}:${min}` //dd/MM/yyyy

    return new Intl.DateTimeFormat(locale).format(movDate)
  }
}

const formatMovementAmt = function (acct, value) {
  return new Intl.NumberFormat(acct.locale, {
    style: 'currency',
    currency: acct.currency,
  }).format(value)
}
//1)Display List of Movements
const displayMovements = function (acct, sort = false) {
  //empty container
  containerMovements.innerHTML = ''

  //Sorting and creating copy of movements using slice
  const movs = sort
    ? acct.movements.slice().sort((a, b) => a - b)
    : acct.movements

  //loop through movements
  movs.forEach((mov, index) => {
    //check type deposit OR withdrawal
    const type = mov > 0 ? 'deposit' : 'withdrawal'

    //Adding Date  to Movements
    const movDate = new Date(acct.movementsDates[index])
    const displayDate = formatMovementDate(movDate, acct.locale)

    //Adding cuurency using INTL Numbers
    const moveAmount = formatMovementAmt(acct, mov)

    //create htmlCode
    const html = `
    <div class="movements__row">
      <div class="movements__type movements__type--${type}">
      ${index + 1} ${type}</div>
      <div class="movements__date">${displayDate}</div>
      <div class="movements__value">${moveAmount}</div>
    </div>
    `
    //add html code using insertAdjacentHTML for movement contaner
    containerMovements.insertAdjacentHTML('afterbegin', html)
  })
}
//2) displayMovements(account1.movements)

//2) Calculate and Print Balance
const calcDispBal = function (acct) {
  acct.balance = acct.movements.reduce((acc, curr) => acc + curr, 0)
  // labelBalance.textContent = `${acct.balance}€`
  labelBalance.textContent = formatMovementAmt(acct, acct.balance)
}
//3) calcDispBal(account1.movements)

//3)Calculate Summary ,income,outcome, and interest labels
const calcDispSummary = function (acct) {
  const incomes = acct.movements
    .filter((mov) => mov > 0)
    .reduce((acc, mov) => acc + mov, 0)

  const outcomes = acct.movements
    .filter((mov) => mov < 0)
    .reduce((acc, mov) => acc + mov, 0)

  const interest = acct.movements
    .filter((mov) => mov > 0)
    .map((deposit) => (deposit * acct.interestRate) / 100)
    .filter((int, i, arr) => int >= 1)
    .reduce((acc, int) => acc + int, 0)

  // labelSumIn.textContent = `${incomes}€`
  // labelSumOut.textContent = `${Math.abs(outcomes)}€`
  // labelSumInterest.textContent = `${interest}€`
  labelSumIn.textContent = formatMovementAmt(acct, incomes)
  labelSumOut.textContent = formatMovementAmt(acct, Math.abs(outcomes))
  labelSumInterest.textContent = formatMovementAmt(acct, interest)
}
// calcDispSummary(account1.movements)

//4)Create USername based on Owner Name (kisan kalani to kk)
const createUsername = function (accsArray) {
  //accounts =[account1, account2, account3, account4]
  accsArray.forEach(function (acc, i) {
    acc.username = acc.owner //adding prop username in accts array
      .toLowerCase()
      .split(' ')
      .map((val) => val[0])
      .join('')
    // console.log(acc.username)
  })
}
createUsername(accounts)
//#region Create Username Practice

//Create Username Practice
// const str = 'Kisan Rajesh Kalani'
// let username = ''

//a) My Method
// const [f, m, l] = str.toLowerCase().split(' ')
// username = f.at(0) + m.at(0) + l.at(0)
// console.log(username)

//b) using for
// for (const [i, val] of str.toLowerCase().split(' ').entries()) {
//   username += val[0]
// }
// console.log(username)

//c) using Map
// const usernmeArr = str
//   .toLowerCase()
//   .split(' ')
//   .map((val) => val[0])
//   .join('')
// console.log(usernmeArr)
//#endregion

const UpdateUI = function (currentAccount) {
  //Display Movements
  displayMovements(currentAccount)

  //DisplayBalance
  calcDispBal(currentAccount)

  //DisplaySummary
  calcDispSummary(currentAccount)
}
let currentAccount

//FAKE Always Logged IN
currentAccount = account1
UpdateUI(currentAccount)
containerApp.style.opacity = 100

//5) Implementing Login Functionality
btnLogin.addEventListener('click', function (e) {
  //1)Prevent form from submitting and Reloading
  e.preventDefault()

  //2)Find User with username
  currentAccount = accounts.find(
    (acc) => acc.username === inputLoginUsername.value.toLowerCase()
  )
  if (currentAccount?.pin === +inputLoginPin.value) {
    // console.log(currentAccount)

    //Display  Message (Login Name)  and  UI(opacity=100)
    labelWelcome.textContent = `Welcome back , ${
      currentAccount.owner.split(' ')[0]
    }`
    containerApp.style.opacity = 100

    //Creating Curretn date and time on Current Balance
    const now = new Date()
    const options = {
      hour: 'numeric',
      minute: 'numeric',
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      // weekday: 'long',
    }
    const formatCurrentBalanceDate = new Intl.DateTimeFormat(
      currentAccount.locale,
      options
    ).format(now)
    labelDate.textContent = formatCurrentBalanceDate

    //Clear Input username and pin fields
    inputLoginUsername.value = inputLoginPin.value = ''
    inputLoginPin.blur()

    //Updating UI
    UpdateUI(currentAccount)
  } else {
    alert('Invalid Username Or Password')
  }
})

//6) Implementing Transfer Functionality
btnTransfer.addEventListener('click', function (e) {
  //1)Prevent form from submitting and Reloading
  e.preventDefault()

  //getting amount and username
  if (inputTransferAmount.value && inputTransferTo.value) {
    const amount = +inputTransferAmount.value
    const receiverAcc = accounts.find(
      (acc) => acc.username === inputTransferTo.value.toLowerCase()
    )
    if (receiverAcc) {
      //Clear Transfer Input
      inputTransferAmount.value = inputTransferTo.value = ''
      if (
        amount > 0 &&
        amount <= currentAccount.balance &&
        receiverAcc?.username !== currentAccount.username
      ) {
        ///Doing the transfer
        currentAccount.movements.push(-amount)
        receiverAcc.movements.push(amount)

        //Add Date to MovementDates
        currentAccount.movementsDates.push(new Date().toISOString())
        receiverAcc.movementsDates.push(new Date().toISOString())

        //Update Current User UI
        UpdateUI(currentAccount)
      } else {
        alert('Check your Balance')
      }
    } else {
      alert('Invalid Username')
    }
  } else {
    alert('Enter Amount OR transfer Username')
  }
})

// 7) Delete account using FindIndex
btnClose.addEventListener('click', function (e) {
  //1)Prevent form from submitting and Reloading
  e.preventDefault()

  //Check User Credentials
  if (
    currentAccount.username === inputCloseUsername.value.toLowerCase() &&
    currentAccount.pin === +inputClosePin.value
  ) {
    console.log('correct user')
    //Find Index of Current User
    const index = accounts.findIndex(
      (acc) => acc.username === currentAccount.username
    )
    console.log(index)

    //Delete User using Splice and Index
    accounts.splice(index, 1)

    //Clear input fields
    inputCloseUsername.value = inputClosePin.value = ''

    //Clear UI ,
    containerApp.style.opacity = 0
  } else {
    alert('Invalid Username OR Password')
  }
})

//8)Loan Button functionality
btnLoan.addEventListener('click', function (e) {
  //1)Prevent form from submitting and Reloading
  e.preventDefault()

  //2)check if amount is gretaer than 10% of any deposited amount
  const amount = Math.floor(inputLoanAmount.value)
  if (
    amount > 0 &&
    currentAccount.movements.some((mov) => mov >= amount * 0.1)
  ) {
    //add into movements
    currentAccount.movements.push(amount)

    //Add Date to Current Acc MovemenetDates
    currentAccount.movementsDates.push(new Date().toISOString())

    //Update UI of Current User
    UpdateUI(currentAccount)
    console.log(currentAccount)
  }
  inputLoanAmount.value = ''
})

//9)Sorting Functionality
//change  displayMovements() add param sort=false and
//Create SortedState to store
let sortState = false
btnSort.addEventListener('click', function (e) {
  //1)Prevent form from submitting and Reloading
  e.preventDefault()

  //sort = true and toggle sortState
  displayMovements(currentAccount, !sortState)
  sortState = !sortState
})

//10)Creating Date on Current Balance
// const now = new Date()
// const day = `${now.getDate()}`.padStart(2, 0)
// const month = `${now.getMonth() + 1}`.padStart(2, 0)
// const year = now.getFullYear()
// const hr = now.getHours()
// const min = `${now.getMinutes()}`.padStart(2, 0)
// labelDate.textContent = `${day}/${month}/${year} ${hr}:${min}` //dd/MM/yyyy
// labelDate.textContent = now.toLocaleDateString() //MM/dd/yyyy

let Practice_problems
//Practice creating programtic Array for Movements on Label Balance Click
// labelBalance.addEventListener('click', function () {
//   const movsArr = Array.from(
//     document.querySelectorAll('.movements__value'),
//     (el) => +el.textContent.replace('€', '')
//   )
//   console.log(movsArr)
// })

//Practice even and odd remainder, coloring even rows ofmovemnets on label click
// labelBalance.addEventListener('click', function () {
//   const que = [...document.querySelectorAll('.movements__row')].forEach(
//     (row, i) => {
//       if (i % 2 === 0) {
//         row.style.backgroundColor = 'orangered'
//       }
//     }
//   )
// })

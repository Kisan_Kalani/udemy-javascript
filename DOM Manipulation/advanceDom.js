'use strict'

///////////////////////////////////////
// Modal window
//#region Query Selectors

const modal = document.querySelector('.modal')
const overlay = document.querySelector('.overlay')
const btnCloseModal = document.querySelector('.btn--close-modal')
const btnsOpenModal = document.querySelectorAll('.btn--show-modal')
const btnScrollTo = document.querySelector('.btn--scroll-to')
const section1 = document.getElementById('section--1')
const nav = document.querySelector('.nav')
const nav_links = document.querySelector('.nav__links')
const slides = document.querySelectorAll('.slide')
const btnSlideLeft = document.querySelector('.slider__btn--left')
const btnSlideRight = document.querySelector('.slider__btn--right')
const dotContainer = document.querySelector('.dots')

//#endregion

//#region  Starting Code
const openModal = function (e) {
  e.preventDefault()
  modal.classList.remove('hidden')
  overlay.classList.remove('hidden')
}

const closeModal = function () {
  modal.classList.add('hidden')
  overlay.classList.add('hidden')
}

btnsOpenModal.forEach((btn) => btn.addEventListener('click', openModal))
// OR  same thing
// for (let i = 0; i < btnsOpenModal.length; i++)
//   btnsOpenModal[i].addEventListener('click', openModal)

btnCloseModal.addEventListener('click', closeModal)
overlay.addEventListener('click', closeModal)

document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal()
  }
})
//#endregion

//#region A) Smooth Scrolling on Learn More to Section--1
btnScrollTo.addEventListener('click', function (e) {
  const secCord = section1.getBoundingClientRect()
  // console.log('section : ', secCord)
  // console.log('btn LearnMore : ', e.target.getBoundingClientRect())

  // console.log('Current Scroll X/Y : ', window.pageXOffset, window.pageYOffset)

  // console.log(
  //   'Doc ViewPrt  H/W :',
  //   document.documentElement.clientHeight,
  //   document.documentElement.clientWidth
  // )

  //Old Method
  //logic section cord positon + current scroll position
  // window.scrollTo({
  //   left: secCord.left + window.pageXOffset,
  //   top: secCord.top + window.pageYOffset,
  //   behavior: 'smooth',
  // })

  //New Method ,supported in new browers
  section1.scrollIntoView({ behavior: 'smooth' })
})
//#endregion

//#region B) Page Navigations from nav links to it's section respective
//adding event to each nav link repeating same func for all
// document.querySelectorAll('.nav__link').forEach((ele) => {
//   ele.addEventListener('click', function (e) {
//     e.preventDefault()
//     const id = this.getAttribute('href')
//     console.log(id)
//     document.querySelector(id).scrollIntoView({ behavior: 'smooth' })
//   })
// })

//Evenet Delegation
// 1)Add evenet listener to Common parent Element
// 2)Determeine what element originated that event
nav_links.addEventListener('click', function (e) {
  //prevent default
  e.preventDefault()

  //Match Startegy
  if (e.target.classList.contains('nav__link')) {
    const id = e.target.getAttribute('href')
    document.querySelector(id).scrollIntoView({ behavior: 'smooth' })
  }
})
//#endregion

//#region C) Tabbed Component
const tabs = document.querySelectorAll('.operations__tab')
const tabContainer = document.querySelector('.operations__tab-container')
const tabContent = document.querySelectorAll('.operations__content')

//👇Wrong method,because if we have 1000 btns then 100 function will create ,use event delegation ,got to parent and the use mathcing
// tabs.forEach((el) =>
//   el.addEventListener('click', (e) => {
//     console.log('Clikced Tab')
//   })
// )

//Using Event Delegeation ,take parent ,use mathcing
tabContainer.addEventListener('click', (e) => {
  // const clickedTab = e.target // gives <span> element also
  // const clickedTab = e.target.parentElement // when clicke on btn ,give container ele,span issue sloved
  const clickedTab = e.target.closest('.operations__tab') //closest gives nearest parent ele

  //Guard Clause
  if (!clickedTab) return

  //remove from all tab btns and Add active class to Cliked Tab
  tabs.forEach((t) => t.classList.remove('operations__tab--active')) //remove active classes from all
  clickedTab.classList.add('operations__tab--active')

  //Activate ContentArea  according to clicked tab
  const clickedDataTabNo = clickedTab.dataset.tab //data-tab=1 in html ,using dataset
  const tabContenttobeshown = document.querySelector(
    `.operations__content--${clickedDataTabNo}`
  )
  tabContent.forEach((tc) => tc.classList.remove('operations__content--active')) //remove active classes from all
  tabContenttobeshown.classList.add('operations__content--active')
})
//#endregion

//#region D) Menu Fade Animation

//event delegation na dmatching
const handleFading = function (e, opacity) {
  if (e.target.classList.contains('nav__link')) {
    const link = e.target
    const siblings = link.closest('nav').querySelectorAll('.nav__link')
    const logo = link.closest('nav').querySelector('img')
    siblings.forEach((el) => {
      if (el !== link) el.style.opacity = this //only for bind ,other wise use oacity variable
    })
    logo.style.opacity = this
  }
}

//Passng parameters to function ,we want to pass opacity to handleFading
//Method 1 -->using inside a function
// nav.addEventListener('mouseover', function (e) {
//   handleFading(e, '0.5')
// })
// nav.addEventListener('mouseout', function (e) {
//   handleFading(e, '1')
// })

//Method 3  --> Using bind opacity will be set to this key word
nav.addEventListener('mouseover', handleFading.bind('0.5'))
nav.addEventListener('mouseout', handleFading.bind('1'))
//#endregion

//#region E) Stikcy Navigation Bar

//Intersection Observer API
//learnign from section 1

// const sect = function (thresholds, observer) {
//   thresholds.forEach((entry) => {
//     console.log(entry.intersectionRatio + '\t' + entry.isIntersecting)
//     console.log(observer)
//   })
// }
// const secOpt = {
//   root: null,
//   threshold: [0, 0.1, 0.4, 1],
// }
// const secObs = new IntersectionObserver(sect, secOpt)
// secObs.observe(section1)
const navHt = nav.getBoundingClientRect()
// console.log(navHt.height)
const stickyNav = function (thresholds) {
  const [threshold] = thresholds
  // console.log(threshold.isIntersecting)
  if (!threshold.isIntersecting) {
    nav.classList.add('sticky')
  } else {
    nav.classList.remove('sticky')
  }
  //
}
const stickyOption = {
  root: null,
  threshold: [0],
  rootMargin: `-${navHt.height}px`,
}
const header = document.querySelector('.header')
const headerObs = new IntersectionObserver(stickyNav, stickyOption)
headerObs.observe(header)
//Old Method --Don't Use
// const intPosition = section1.getBoundingClientRect()
// // console.log(intPosition)
// window.addEventListener('scroll', function () {
//   console.log(window.scrollY)
//   window.scrollY > intPosition.top
//     ? nav.classList.add('sticky')
//     : nav.classList.remove('sticky')
// })

//#endregion

//#region F) Revealing sections on scroll using Intersection Onserver API
const allSections = document.querySelectorAll('.section')
const secFunc = function (entries, observer) {
  const [entry] = entries
  // console.log(entry)
  if (!entry.isIntersecting) return
  entry.target.classList.remove('section--hidden')
  observer.unobserve(entry.target)
}
const secOpt = {
  root: null,
  threshold: 0.2,
}
const sectionObs = new IntersectionObserver(secFunc, secOpt)
allSections.forEach((section) => {
  sectionObs.observe(section)
  // section.classList.add('section--hidden')
})
//#endregion

//#region G) Lazy Loading Images

const allBlurImgs = document.querySelectorAll('img[data-src]')
// console.log(allBlurImgs)
const imgFunc = (entries, observer) => {
  const [entry] = entries

  if (!entry.isIntersecting) return
  entry.target.src = entry.target.dataset.src
  entry.target.addEventListener('load', function () {
    entry.target.classList.remove('lazy-img')
  })
  observer.unobserve(entry.target)
}
const imgObs = new IntersectionObserver(imgFunc, {
  root: null,
  threshold: 1,
})
allBlurImgs.forEach((img) => imgObs.observe(img))
//#endregion

//#region F) Slider Functionality
//just for visibiliy of images ,delete later
// const slider = document.querySelector('.slider')
// slider.style.overflow = 'visible'
// slider.style.transform = 'scale(0.7) translateX(-1400px)'

//Slides
let curSlideIndex = 0
const maxSlides = slides.length
const init = function () {
  goToSlide(0)
  createDots()
  activateDots(0)
}

const goToSlide = function (slide) {
  slides.forEach((s, i) => {
    s.style.transform = `translateX(${100 * (i - slide)}%)` //0% ,100% ,200%,300%
  })
}
const nextSlide = () => {
  // need -100% , 0% , 100%, 200%
  // curSlideIndex = curSlideIndex === maxSlides - 1 ? 0 : curSlideIndex + 1
  if (curSlideIndex === maxSlides - 1) {
    curSlideIndex = 0
  } else {
    curSlideIndex++
  }
  goToSlide(curSlideIndex)
  activateDots(curSlideIndex)
}
const prevSlide = () => {
  if (curSlideIndex == 0) {
    curSlideIndex = maxSlides - 1
  } else {
    curSlideIndex--
  }
  goToSlide(curSlideIndex)
  activateDots(curSlideIndex)
}
//Button Click Left and Right
btnSlideRight.addEventListener('click', nextSlide)
btnSlideLeft.addEventListener('click', prevSlide)

//Using keyboard arrows keys
document.addEventListener('keydown', function (e) {
  if (e.key === 'ArrowRight') {
    nextSlide()
  }
  if (e.key === 'ArrowLeft') {
    prevSlide()
  }
})

//Adding DOTS
const activateDots = (slide) => {
  document.querySelectorAll('.dots__dot').forEach((s, i) => {
    s.classList.remove('dots__dot--active')
  })
  document
    .querySelector(`.dots__dot[data-slide='${slide}']`)
    .classList.add('dots__dot--active')
}

const createDots = function () {
  slides.forEach((_, i) => {
    const dotHTML = `<button class="dots__dot" data-slide="${i}"></button>`
    dotContainer.insertAdjacentHTML('beforeend', dotHTML)
  })
}
dotContainer.addEventListener('click', function (e) {
  if (e.target.classList.contains('dots__dot')) {
    const slide = e.target.dataset.slide

    goToSlide(slide)
    activateDots(slide)
  }
})

init()
//#endregion

//#region Learning and Practice Section
let Selecting_Creating_Deleting_Elements
//Selecting Elements
// console.log(document.documentElement) //whole html page
// console.log(document.head)
// console.log(document.body)

// const allSection = document.querySelectorAll('.section')
// console.log(allSection) //NodeList

// document.getElementById('section--1')

// const allButtons = document.getElementsByTagName('button')
// console.log(allButtons) //HTMLCollections

// document.getElementsByClassName('btn')

//Creating and Inserting Elements
// Method 1) insertAdjacentHTML() //check banklist_arrays
//2) Cookie example
// const header = document.querySelector('.header')
// const message = document.createElement('div')
// message.classList.add('cookie-message')
// message.innerHTML =
//   'We got a Cookie Message example for Creating Element. <buttton class="btn btn--close-cookie">Got it</button>'

//using prepend ,append
// header.prepend(message) //at beginning
// header.append(message) //at end

// header.append(message.cloneNode(true)) //at get at to pand end both ,use prpend then append with cloneNode(true)

//using before,after
// header.before(message) //before the header
// header.after(message) //after the header

//Deleting Element
// document
//   .querySelector('.btn--close-cookie')
//   .addEventListener('click', function () {
//     message.remove()
//   })

let Styles_Attributes_Classes //   using same cookie-message example
//A) Styles
//using inLine
// message.style.backgroundColor = '#37383d'
// message.style.width = '120%'

// console.log(message.style.height) //nothing on console, can only print inlin estyle that we declared
// console.log(message.style.backgroundColor)

//to get styles use getComputedStyles(message)
// console.log(getComputedStyle(message).height) //prints height = >55.3828px

// alter message syle ,height
// message.style.height =
//   Number.parseFloat(getComputedStyle(message).height, 10) + 40 + 'px'

//using setProperty
// document.documentElement.style.setProperty('--color-primary', 'orangered')
// message.style.setProperty('background-color', '#37383d')

// B) Attributes
// const logo = document.querySelector('.nav__logo')
// console.log(logo.alt) //Bankist logo
// console.log(logo.src) // img url  http://127.0.0.1:5500/DOM%20Manipulation/img/logo.png
// console.log(logo.className) //nav__logo
// console.log(logo.designer) //undefined , does not work on custom attributes
// //Get and Set Attribute
// console.log(logo.getAttribute('designer')) //Kisan
// console.log(logo.getAttribute('src')) //relative link - >DOM%20Manipulation/img/logo.png
// logo.setAttribute('company', 'Bankist')

//Data attributes check data-version-number
// console.log(logo.dataset.versionNumber) //3.0 ,use Camel case

//C)  Classes
// logo.classList.add()
// logo.classList.remove()
// logo.classList.toggle()
// logo.classList.contains()

let Events_TypesOfEvents_EventPropogation
// const h1 = document.querySelector('h1')
// const funcEnter = function () {
//   alert('mousetemter happened!!')
//   h1.removeEventListener('mouseenter', funcEnter)
// }
// h1.addEventListener('mouseenter', funcEnter)

// const randomInt = (min, max) =>
//   Math.floor(Math.random() * (max - min + 1) + min)
// const randomColor = () =>
//   `rgb(${randomInt(0, 255)},${randomInt(0, 255)},${randomInt(0, 255)})`
// console.log(randomColor())

// document.querySelector('.nav__link').addEventListener('click', function (e) {
//   e.stopPropagation()
//   this.style.backgroundColor = randomColor()
// })
// document.querySelector('.nav__links').addEventListener('click', function (e) {
//   e.stopPropagation()
//   this.style.backgroundColor = randomColor()
// })
// document.querySelector('.nav').addEventListener('click', function (e) {
//   this.style.backgroundColor = randomColor()
// })

let DOM_Traversing
// const h1 = document.querySelector('h1')

//Going Downwward Child
// console.log(h1.querySelectorAll('.highlight')) //NodeList
// console.log(h1.childNodes) //NodeList
// console.log(h1.children) //HTML Collection
// h1.firstElementChild.style.color = 'white'
// h1.lastElementChild.style.color = 'orangered'

//Going Upward parent
// console.log(h1.parentNode)
// console.log(h1.parentElement)
// h1.closest('.header').style.background = 'lightgrey'

//Going Sideways ,Siblings only left or right
// console.log(h1.previousElementSibling)
// console.log(h1.nextElementSibling)

//for all siblings ,go to parent then childrens
// console.log(h1.parentElement.children)
// ;[...h1.parentElement.children].forEach((el) => {
//   if (el !== h1) el.style.transform = 'scale(0.6)'
// })

//#endregion

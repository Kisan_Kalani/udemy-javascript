'use strict'

//https://pig-game-v2.netlify.app/

const player0El = document.querySelector('.player--0')
const player1El = document.querySelector('.player--1')

const current0EL = document.getElementById('current--0')
const current1EL = document.getElementById('current--1')

const score0_EL = document.getElementById('score--0')
const score1_EL = document.getElementById('score--1')

var diceEl = document.querySelector('.dice')
const btnNew = document.querySelector('.btn--new')
const btnRoll = document.querySelector('.btn--roll')
const btnHold = document.querySelector('.btn--hold')

let currentScore, activeplayer, scored, playing

const init = function () {
  scored = [0, 0]
  currentScore = 0
  activeplayer = 0
  playing = true

  score0_EL.textContent = 0
  score1_EL.textContent = 0
  current0EL.textContent = 0
  current1EL.textContent = 0
  diceEl.classList.add('hidden')
  player0El.classList.remove('player--winner')
  player1El.classList.remove('player--winner')
  player0El.classList.add('player--active')
  player1El.classList.remove('player--active')
}

const switchPlayer = function () {
  document.getElementById(`current--${activeplayer}`).textContent = 0
  currentScore = 0
  activeplayer = activeplayer === 0 ? 1 : 0

  player0El.classList.toggle('player--active')
  player1El.classList.toggle('player--active')
}

init()

btnHold.addEventListener('click', function () {
  if (playing) {
    scored[activeplayer] += currentScore

    document.getElementById(`score--${activeplayer}`).textContent =
      scored[activeplayer]

    if (scored[activeplayer] >= 20) {
      playing = false
      document
        .querySelector(`.player--${activeplayer}`)
        .classList.add('player--winner')

      document
        .querySelector(`.player--${activeplayer}`)
        .classList.remove('player--active')

      diceEl.classList.add('hidden')
    } else {
      switchPlayer()
    }
  }
})

btnRoll.addEventListener('click', function () {
  if (playing) {
    const diceRandomNo = Math.trunc(Math.random() * 6) + 1

    diceEl.classList.remove('hidden')
    diceEl.src = `Pig Game/dice-${diceRandomNo}.png`

    if (diceRandomNo != 1) {
      currentScore += diceRandomNo
      document.getElementById(`current--${activeplayer}`).textContent =
        currentScore
    } else {
      switchPlayer()
    }
  }
})
btnNew.addEventListener('click', init)

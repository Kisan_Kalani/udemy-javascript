'use strict'

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data

const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],
  interestRate: 1.2, // %
  pin: 1111,
  movementsDates: [
    '2019-11-18T21:31:17.178Z',
    '2019-12-23T07:42:02.383Z',
    '2020-01-28T09:15:04.904Z',
    '2020-04-01T10:17:24.185Z',
    '2020-05-08T14:11:59.604Z',
    '2022-10-28T17:01:17.194Z',
    '2022-10-30T23:36:17.929Z',
    '2022-11-11T10:51:36.790Z',
  ],
  currency: 'EUR',
  locale: 'pt-PT', // de-DE
}

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
}

const accounts = [account1, account2]

//#region  Selectors

const labelWelcome = document.querySelector('.welcome')
const labelDate = document.querySelector('.date')
const labelBalance = document.querySelector('.balance__value')
const labelSumIn = document.querySelector('.summary__value--in')
const labelSumOut = document.querySelector('.summary__value--out')
const labelSumInterest = document.querySelector('.summary__value--interest')
const labelTimer = document.querySelector('.timer')

const containerApp = document.querySelector('.app')
const containerMovements = document.querySelector('.movements')

const btnLogin = document.querySelector('.login__btn')
const btnTransfer = document.querySelector('.form__btn--transfer')
const btnLoan = document.querySelector('.form__btn--loan')
const btnClose = document.querySelector('.form__btn--close')
const btnSort = document.querySelector('.btn--sort')

const inputLoginUsername = document.querySelector('.login__input--user')
const inputLoginPin = document.querySelector('.login__input--pin')
const inputTransferTo = document.querySelector('.form__input--to')
const inputTransferAmount = document.querySelector('.form__input--amount')
const inputLoanAmount = document.querySelector('.form__input--loan-amount')
const inputCloseUsername = document.querySelector('.form__input--user')
const inputClosePin = document.querySelector('.form__input--pin')

//#endregion
//////////////////////////////////////////////////////////////////////////////////

//Display Moevments

const formatDate = function (movDate, locale) {
  const calcdays = (date1, date2) =>
    Math.round(Math.abs((date2 - date1) / (1000 * 60 * 60 * 24)), 0)

  const days = calcdays(movDate, new Date())
  if (days === 0) {
    return 'Today'
  } else if (days === 1) {
    return 'Yesterday'
  } else if (days <= 7) {
    return `${days} days ago`
  } else {
    return new Intl.DateTimeFormat(locale).format(new Date(movDate))
  }
}
let sort = false
const displayMovements = function (acct, sort) {
  containerMovements.textContent = ''
  const mov = sort ? acct.movements.slice().sort() : acct.movements
  mov.forEach((mov, i) => {
    const type = mov > 0 ? 'deposit' : 'withdrawal'

    const movdate = new Date(acct.movementsDates[i])
    const dispDate = formatDate(movdate, acct.locale)

    const html = `
    <div class="movements__row">
      <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type}</div>
      <div class="movements__date">${dispDate}</div>
      <div class="movements__value">${mov}€</div>
    </div>
  `
    containerMovements.insertAdjacentHTML('afterbegin', html)
  })
}
//Display Balance
const displayBalance = function (acct) {
  const bal = acct.movements.reduce((acc, sum) => {
    return acc + sum
  })
  acct.balance = bal

  // const now = new Date()
  // const day = now.getDate()
  // const month = now.getMonth() + 1
  // const year = now.getFullYear()
  // console.log(day, month, year)

  const options = {
    hour: 'numeric',
    minute: 'numeric',
    // second: 'numeric',
    day: 'numeric',
    month: 'numeric',
    year: 'numeric',
  }
  const now = new Intl.DateTimeFormat(acct.locale, options).format(new Date())
  labelDate.textContent = now
  labelBalance.textContent = `${bal}€`
}
//Display Summary
const displaySummary = function (acct) {
  const totalDeposit = acct.movements
    .filter((m) => m > 0)
    .reduce((acc, sum) => acc + sum, 0)

  const totalWithdrawal = acct.movements
    .filter((m) => m < 0)
    .reduce((acc, sum) => acc + sum, 0)

  const totalInterest = acct.movements
    .filter((m) => m > 0)
    .map((m) => (m * acct.interestRate) / 100)
    .filter((int, i, arr) => {
      return int >= 1
    })
    .reduce((acc, sum) => acc + sum, 0)

  labelSumIn.textContent = `${totalDeposit.toFixed(2)}€`
  labelSumOut.textContent = `${Math.abs(totalWithdrawal).toFixed(2)}€`
  labelSumInterest.textContent = `${totalInterest.toFixed(2)}€`
}

const UpdateUI = function (currentAccount) {
  //Display Movements
  displayMovements(currentAccount, sort)

  //DisplayBalance
  displayBalance(currentAccount)

  //DisplaySummary
  displaySummary(currentAccount)
}
//Create Username
const CreateUsername = function () {
  accounts.forEach((acc) => {
    const username = acc.owner
      .toLowerCase()
      .split(' ')
      .map((o) => o[0])
      .join('')
    acc.username = username
  })
}
//Init
const init = function () {
  // containerApp.style.opacity = 0
  inputLoginUsername.value = inputLoginPin.value = ''
  CreateUsername()
}
init()
let currentLoggedInAcct = account1
UpdateUI(currentLoggedInAcct)

btnLogin.addEventListener('click', function (e) {
  e.preventDefault()
  const username = inputLoginUsername.value
  const pin = +inputLoginPin.value
  const currentUser = accounts.find(
    (u) => u.username === username && u.pin === pin
  )

  if (!currentUser) {
    alert('Please enter Correct Username Or Password')
    return
  }
  currentLoggedInAcct = currentUser
  UpdateUI(currentLoggedInAcct)
  labelWelcome.textContent = `Good Day, ${
    currentLoggedInAcct.owner.split(' ')[0]
  }!`
  containerApp.style.opacity = 1
})

btnTransfer.addEventListener('click', function (e) {
  e.preventDefault()
  console.log(currentLoggedInAcct)
  const trnTo = inputTransferTo.value
  const trnAmt = +inputTransferAmount.value
  inputTransferTo.value = inputTransferAmount.value = ''
  const user = accounts.find(
    (u) =>
      u.username == trnTo.trim().toLowerCase() &&
      u.username !== currentLoggedInAcct.username
  )
  if (trnTo && trnAmt) {
    if (user) {
      if (trnAmt <= Math.trunc(currentLoggedInAcct.balance)) {
        console.log(trnTo, trnAmt, Math.trunc(currentLoggedInAcct.balance))

        user.movements.push(trnAmt)
        user.movementsDates.push(new Date())

        currentLoggedInAcct.movements.push(-trnAmt)
        currentLoggedInAcct.movementsDates.push(new Date())

        UpdateUI(currentLoggedInAcct)
      } else {
        alert('transfer amount greater than Current Balance')
      }
    } else {
      alert('Enter Valid Username')
    }
  } else {
    alert('Enter Transfer Details')
  }
})

btnLoan.addEventListener('click', function (e) {
  e.preventDefault()
  const loan = +inputLoanAmount.value
  inputLoanAmount.value = ''

  if (
    loan > 0 &&
    currentLoggedInAcct.movements.some((mov) => mov >= loan * 0.1)
  ) {
    setTimeout(() => {
      currentLoggedInAcct.movements.push(loan)
      currentLoggedInAcct.movementsDates.push(new Date())
      UpdateUI(currentLoggedInAcct)
    }, 2000)
  } else {
    alert('Sorry! ,,Credit is low ')
  }
})

btnClose.addEventListener('click', function (e) {
  e.preventDefault()
  const closeUsrname = inputCloseUsername.value
  const closePin = +inputClosePin.value

  inputCloseUsername.value = inputClosePin.value = ''

  const acc = accounts.find(
    (acc) =>
      acc.username === closeUsrname &&
      acc.pin === closePin &&
      acc === currentLoggedInAcct
  )
  if (!acc) return

  accounts.splice(accounts.indexOf(acc), 1)
  init()
  currentLoggedInAcct = null
})

btnSort.addEventListener('click', function (e) {
  e.preventDefault()
  displayMovements(currentLoggedInAcct, sort)
  sort = !sort
})

//Timer Functionality

// let cnt = 10
// const time = setInterval(() => {
//   if (cnt) console.log(cnt)

//   labelTimer.textContent = cnt
//   cnt--
// }, 1000)

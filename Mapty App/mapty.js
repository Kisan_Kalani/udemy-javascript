'use strict'

// prettier-ignore
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const form = document.querySelector('.form')
const containerWorkouts = document.querySelector('.workouts')
const inputType = document.querySelector('.form__input--type')
const inputDistance = document.querySelector('.form__input--distance')
const inputDuration = document.querySelector('.form__input--duration')
const inputCadence = document.querySelector('.form__input--cadence')
const inputElevation = document.querySelector('.form__input--elevation')

class Workout {
  date = new Date()
  id = (Date.now() + '').slice(-8)

  constructor(coords, distance, duration) {
    this.coords = coords
    this.distance = distance
    this.duration = duration
  }

  _setDesciption() {
    this.description = ` ${
      this.type === 'running' ? '🏃‍♂️' : '🚴‍♀️'
    } ${this.type[0].toUpperCase()}${this.type.slice(1)} on ${months.find(
      (m, i) => i === this.date.getMonth()
    )} ${this.date.getDate()}`
  }
}

class Running extends Workout {
  type = 'running'
  constructor(coords, distance, duration, cadence) {
    super(coords, distance, duration)
    this.cadence = cadence
    this.calcPace()
    this._setDesciption()
  }

  calcPace() {
    this.pace = this.duration / this.distance
    return this.pace
  }
}

class Cycling extends Workout {
  type = 'cycling'
  constructor(coords, distance, duration, elevationGain) {
    super(coords, distance, duration)
    this.elevationGain = elevationGain
    this.calcSpeed()
    this._setDesciption()
  }

  calcSpeed() {
    this.speed = this.distance / (this.duration / 60)
    return this.speed
  }
}

class App {
  #map
  #mapEvent
  #zoom = 13
  #workouts = []
  constructor() {
    //get USer Postions
    this._getPosition()

    //workouts from localstorage
    this._getLocalStorage()

    //Event Listeners
    inputType.addEventListener('change', this._toggleElevationForm)
    form.addEventListener('submit', this._newWorkout.bind(this))
    containerWorkouts.addEventListener('click', this._moveToPopup.bind(this))
  }

  _getPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        this._loadMap.bind(this),
        this._alertError
      )
    }
  }

  _loadMap(position) {
    const { latitude, longitude } = position.coords //get latitude and longitude
    // console.log(latitude, longitude)
    //got to google maps and copy url https://www.google.com/maps/@18.8154265,76.7747674,7z , delete last , sentence
    // console.log(`https://www.google.com/maps/@${latitude},${longitude}`)

    //Leaflet api for marker
    const coords = [latitude, longitude]
    this.#map = L.map('map').setView(coords, this.#zoom)

    // https://tile.openstreetmap.org/{z}/{x}/{y}.png
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.#map)

    //Map Click
    this.#map.on('click', this._showForm.bind(this))

    //render on map  from getlocalstorage
    this.#workouts.forEach((work) => {
      this._renderWorkoutMarkup(work)
    })
  }

  _showForm(mapE) {
    this.#mapEvent = mapE
    form.classList.remove('hidden')
    inputDistance.focus()
  }

  _hideForm() {
    this._clearInputs()
    form.style.display = 'none'
    form.classList.add('hidden')
    setTimeout(() => (form.style.display = 'grid'), 1000)
  }

  _alertError(err) {
    alert('Cannot locate position ', err.message)
  }

  _clearInputs() {
    inputDistance.value = ''
    inputDuration.value = ''
    inputCadence.value = ''
    inputElevation.value = ''
  }

  _toggleElevationForm() {
    inputElevation.closest('.form__row').classList.toggle('form__row--hidden')
    inputCadence.closest('.form__row').classList.toggle('form__row--hidden')
  }

  _newWorkout(e) {
    e.preventDefault()

    //get input data
    const type = inputType.value
    const distance = +inputDistance.value
    const duration = +inputDuration.value
    const { lat, lng } = this.#mapEvent.latlng

    //Validate Input data
    // if (this._validateInput(type, distance, duration, cadence, elevationGain))  //My Method
    const validInputs = (...inputs) =>
      inputs.every((inp) => Number.isFinite(inp))
    const allPositive = (...inputs) => inputs.every((inp) => inp > 0)

    let workout
    //if workout is Running, create Running Object
    if (type === 'running') {
      const cadence = +inputCadence.value
      if (
        !validInputs(distance, duration, cadence) ||
        !allPositive(distance, duration, cadence)
      ) {
        alert('Enter Valid Input')
        this._clearInputs()
        return
      }
      workout = new Running([lat, lng], distance, duration, cadence)
    }

    //if workout is Cycling, create Cycling Object
    if (type === 'cycling') {
      const elevationGain = +inputElevation.value
      if (
        !validInputs(distance, duration, elevationGain) ||
        !allPositive(distance, duration)
      ) {
        alert('Enter Valid Input')
        this._clearInputs()
        return
      }

      workout = new Cycling([lat, lng], distance, duration, elevationGain)
    }

    this.#workouts.push(workout)

    // Displaying  Marker on Map
    this._renderWorkoutMarkup(workout)

    //display on list
    this._renderWorkoutList(workout)

    //Hide Form
    this._hideForm()

    //set LocalStorage
    this._setLocalStorage()
  }

  _renderWorkoutMarkup(workout) {
    //L.marker([lat, lng]).addTo(map).bindPopup('Your Location').openPopup()
    L.marker(workout.coords)
      .addTo(this.#map)
      .bindPopup(
        L.popup({
          maxWidth: 250,
          minWidth: 100,
          autoClose: false,
          closeOnClick: false,
          className: `${workout.type}-popup`,
        })
      )
      .setPopupContent(workout.description)
      .openPopup()
  }

  _renderWorkoutList(workout) {
    let htmlBody = `
    <li class="workout workout--${workout.type}" data-id="${workout.id}">
    <h2 class="workout__title">${workout.description}</h2>
    <div class="workout__details">
        <span class="workout__icon">${
          workout.type === 'running' ? '🏃‍♂️' : '🚴‍♀️'
        }</span>
        <span class="workout__value">${workout.distance}</span>
        <span class="workout__unit">km</span>
    </div>
    <div class="workout__details">
        <span class="workout__icon">⏱</span>
        <span class="workout__value">${workout.duration}</span>
        <span class="workout__unit">min</span>
    </div>
    `

    if (workout.type === 'running') {
      htmlBody += `
      <div class="workout__details">
      <span class="workout__icon">⚡️</span>
      <span class="workout__value">${workout.pace.toFixed(1)}</span>
      <span class="workout__unit">min/km</span>
      </div>
      <div class="workout__details">
      <span class="workout__icon">🦶🏼</span>
      <span class="workout__value">${workout.cadence}</span>
      <span class="workout__unit">spm</span>
      </div>
      `
    }

    if (workout.type === 'cycling') {
      htmlBody += `
      <div class="workout__details">
        <span class="workout__icon">⚡️</span>
        <span class="workout__value">${workout.speed.toFixed(1)}</span>
        <span class="workout__unit">km/h</span>
      </div>
      <div class="workout__details">
        <span class="workout__icon">⛰</span>
        <span class="workout__value">${workout.elevationGain}</span>
        <span class="workout__unit">m</span>
      </div>
      `
    }
    document.querySelector('ul').insertAdjacentHTML('beforeend', htmlBody)
  }

  _moveToPopup(e) {
    const workoutEl = e.target.closest('.workout')

    console.log(workoutEl)
    if (!workoutEl) return

    const workout = this.#workouts.find(
      (work) => work.id == workoutEl.dataset.id
    )
    console.log(workout)

    this.#map.setView(workout.coords, this.#zoom, {
      animate: true,
      pan: { duration: 1 },
    })
  }
  _validateInput(type, distance, duration, cadence, elevationGain) {
    //#region My Logic
    if (
      !Number.isFinite(distance) &&
      distance > 0 &&
      !Number.isFinite(duration) &&
      duration > 0 &&
      !Number.isFinite(cadence) &&
      !Number.isFinite(elevationGain) &&
      ((type === 'running' && cadence > 0) ||
        (type === 'cycling' && elevationGain > 0))
    ) {
      return true
    } else {
      return false
    }
    //#endregion
  }

  _setLocalStorage() {
    localStorage.setItem('workouts', JSON.stringify(this.#workouts))
  }

  _getLocalStorage() {
    const data = JSON.parse(localStorage.getItem('workouts'))

    if (!data) return

    this.#workouts = data

    //render on list from getlocalstorage
    this.#workouts.forEach((work) => {
      this._renderWorkoutList(work)
    })
  }

  reset() {
    localStorage.removeItem('workouts')
    location.reload()
  }
}

const app = new App()

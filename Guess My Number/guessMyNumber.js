'use strict'

var message = document.querySelector('.message')
var secretNumber = document.querySelector('.number')
var scroe = document.querySelector('span.score') //scroe inside of span
var highscore = document.querySelector('span.highscore') //scroe inside of span
var txtNumber = document.querySelector('.guess')
var body = document.querySelector('body')
var btnClick = document.querySelector('#btnClick')
var btnAgain = document.querySelector('#btnAgain')
// console.log(message.textContent)
// console.log(scroe.textContent)
// console.log(highscore.textContent)
// console.log(txtNumber.value) //empty value for inoput
// console.log(btnClick)

// message.textContent = '🎁 Correct Number'
// secretNumber.textContent = 15
// scroe.textContent = 10
// highscore.textContent = 24
// txtNumber.value = 6
let randomNumber = Math.trunc(Math.random() * 20 + 1) //random NO bet 1-20
// secretNumber.textContent = randomNumber
let startingScroe = 20
let varhighscroe = 0

// const onClick = function () {
//   console.log(txtNumber.value) //cl on number input field
// }
// btnClick.addEventListener('click', onClick)
//   ---------------------OR---------------------
btnClick.addEventListener('click', function () {
  const enteredNumber = Number(txtNumber.value)

  if (!enteredNumber) {
    message.textContent = '🎃No Number!!'
  } else if (enteredNumber === randomNumber) {
    message.textContent = '🏆Correct Number'
    secretNumber.textContent = randomNumber
    //styling on correct answer
    body.style.backgroundColor = '#60b347'
    secretNumber.style.width = '30rem'

    //setting highscore
    if (scroe.textContent > varhighscroe) {
      varhighscroe = scroe.textContent
      highscore.textContent = varhighscroe
    }
  } else if (enteredNumber > randomNumber) {
    if (startingScroe > 1) {
      message.textContent = '📈Too High!!'
      startingScroe--
      scroe.textContent = startingScroe
    } else {
      message.textContent = '🛠 You Lost The Game!!'
      scroe.textContent = 0
    }
  } else if (enteredNumber < randomNumber) {
    if (startingScroe > 1) {
      message.textContent = '📉Too Low!!'
      startingScroe--
      scroe.textContent = startingScroe
    } else {
      message.textContent = '🛠 You Lost The Game!!'
      scroe.textContent = 0
    }
  }
  //   txtNumber.value = ''
})

//Again Click
const onAgainClick = function () {
  message.textContent = 'Start guessing...'
  randomNumber = Math.trunc(Math.random() * 20 + 1)
  scroe.textContent = 20
  secretNumber.textContent = '?'
  txtNumber.value = ''
  body.style.backgroundColor = '#222'
  secretNumber.style.width = '15rem'
}
btnAgain.addEventListener('click', onAgainClick)

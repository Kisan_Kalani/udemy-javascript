'use strict'

const btn = document.querySelector('.btn-country')
const countriesContainer = document.querySelector('.countries')
const baseUrl = 'https://restcountries.com/v3.1/'

const loadCountryHtml = function (data, className = '') {
  const [...lan] = Object.values(data.languages)
  const [currency] = Object.values(data.currencies)
  let html = `
  <article class="country ${className}">
  <img class="country__img" src="${data.flags.png}" />
  <div class="country__data">
  <h3 class="country__name">${data.name.common}</h3>
  <h4 class="country__region">${data.region}</h4>
  <p class="country__row"><span>👫</span>${lan}</p>
  <p class="country__row"><span>🗣️</span>${(data.population / 10000).toFixed(
    1
  )}</p>
  <p class="country__row"><span>💰</span>${currency.name}</p>
  </div>
  </article>
  `
  countriesContainer.insertAdjacentHTML('beforeend', html)
  countriesContainer.style.opacity = 1
}
///////////////////////////////////////

//#region XMLHttpRequest Old Method for Async Ajax

// const getCountry = function (country) {
//   const request = new XMLHttpRequest()
//   //api => get by country name ->https://restcountries.com/v3.1/name/{name}
//   request.open('GET', baseUrl + 'name/' + country)
//   request.send()
//   //   console.log(request.responseText) //empty
//   request.addEventListener('load', function () {
//     const [data] = JSON.parse(this.responseText) //used destructuring as only one data in response
//     console.log(data)

//     const [...lan] = Object.values(data.languages)
//     const [currency] = Object.values(data.currencies)

//     let html = `
//         <article class="country">
//         <img class="country__img" src="${data.flags.png}" />
//         <div class="country__data">
//         <h3 class="country__name">${data.name.common}</h3>
//         <h4 class="country__region">${data.region}</h4>
//         <p class="country__row"><span>👫</span>${lan}</p>
//         <p class="country__row"><span>🗣️</span>${(
//           data.population / 10000
//         ).toFixed(1)}</p>
//         <p class="country__row"><span>💰</span>${currency.name}</p>
//         </div>
//         </article>
//         `
//     countriesContainer.insertAdjacentHTML('beforeend', html)
//     countriesContainer.style.opacity = 1
//   })
// }
// getCountry('portugal')
// getCountry('india')
// getCountry('usa')
//#endregion

//#region Callback Hell

// const getCountryAndNeighbour = function (country) {
//   const request1 = new XMLHttpRequest()
//   //api => get by country name ->https://restcountries.com/v3.1/name/{name}
//   request1.open('GET', baseUrl + 'name/' + country)
//   request1.send()
//   //   console.log(request.responseText) //empty
//   request1.addEventListener('load', function () {
//     const [data] = JSON.parse(this.responseText) //used destructuring as only one data in response
//     console.log(data)
//     loadCountryHtml(data)

//     const [neighbour] = data.borders

//     if (!neighbour) return

//     const request2 = new XMLHttpRequest()
//     //api => get by country name ->https://restcountries.com/v3.1/name/{name}
//     request2.open('GET', baseUrl + 'alpha/' + neighbour)
//     request2.send()
//     //   console.log(request.responseText) //empty
//     request2.addEventListener('load', function () {
//       const [neighbour] = JSON.parse(this.responseText)
//       console.log(neighbour)

//       loadCountryHtml(neighbour, 'neighbour')
//     })
//   })
// }
// getCountryAndNeighbour('india')
//#endregion

//#region  Consume Promises and fetch api
// const getCountryData = function (country) {
//   fetch(baseUrl + `name/${country}`)
//     .then(
//       (response) => {
//         // console.log(response) //does not contain data ,it is readable stream to convert use resp.json() and return  it

//         if (!response.ok) {
//           throw new Error('country not found')
//         }
//         return response.json() //returns another promise
//       }
//       // (error) => alert('response1', error)
//     )
//     .then((data) => {
//       const [country] = data
//       loadCountryHtml(country) //load portugal
//       const [border] = country.borders

//       //get neigbour
//       return fetch(baseUrl + `alpha/${border}`) //returns promise
//     })
//     .then((neiResp) => {
//       return neiResp.json() //returns another promise
//     })
//     .then((neiData) => {
//       const [neighbour] = neiData
//       loadCountryHtml(neighbour, 'neighbour') //load spain
//     })
//     .catch((err) => alert(err))
//     .finally((x) => {
//       console.warn('finnally')
//     })
// }

// getCountryData('india')
//#endregion

//#region   Making Our Own Promise
//use resolve and reject
//use then adn catch on promise obj created
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    const rand = Math.random()
    if (rand >= 0.5) {
      resolve('You Win')
    } else {
      reject('You Lost')
    }
  }, 2000)
})
promise.then((resp) => console.log(resp)).catch((err) => console.warn(err))
//#endregion

//Use of Async and Await
const asyncFunc = async function () {
  console.log('error code asd')
  //fetc api call
  await fetch('')
}

const normal = function () {
  console.log('Normal')
}

console.log('FIRST')
asyncFunc()
console.log('LAST')
